<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Cache;

use Nomad\Core as Core;
use Nomad\Exception as Exception;

/**
 * Class Factory
 * Factory cache class
 *
 * @package Nomad\Cache
 */
class Factory
    extends Core\BaseClass
{
    /**
     * @var string type of class to create
     */
    protected $_type;

    /**
     * @var mixed Options
     */
    protected $_options;

	/**
	 * @param array $params
	 * @return Driver\File|Driver\Memcached
	 * @throws Exception\Argument
	 * @throws Exception\Service
	 * @throws \Exception
	 */
    public function initialize($params = array())
    {
        if (!$this->_type) {
            throw new Exception\Argument("Cache type cannot be empty");
        }

        switch ($this->_type) {
            case "memcached":
            {
                $memcached = new Driver\Memcached($this->_options);
                $memcached->connect();
                return $memcached;

                break;
            }
            case "file":
                if (is_array($this->_options)) {
                    $params = array_merge($this->_options, $params);
                }
                $fileCache = new Driver\File($params);
                return $fileCache;
                break;
            default:
                {
                throw new Exception\Argument("Unknown Cache type: {$this->_type}.");
                }
        }
    }
}