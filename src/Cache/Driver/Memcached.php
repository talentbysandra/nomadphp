<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Cache\Driver;

use Nomad\Cache as Cache;
use Nomad\Exception as Exception;

/**
 * Class Memcached
 *
 * @package Nomad\Cache\Driver
 * @author  Mark Hillebert
 */
class Memcached
	extends Cache\Driver
{
	/**
	 * @const int
	 */
	const _DEFAULT_DURATION = 120;

	/**
	 * @var
	 */
	protected $_service;

	/**
	 * @var string
	 */
	protected $_host = 'localhost';

	/**
	 * @var string
	 */
	protected $_port = '11211';

	/**
	 * @var bool
	 */
	protected $_isConnected = false;

	/**
	 * @return bool
	 */
	protected function _isValidService()
	{
		$isEmpty    = empty($this->_service);
		$isInstance = $this->_service instanceof \Memcache;
		if ($this->_isConnected && $isInstance && !$isEmpty) {
			return true;
		}

		return false;
	}

	/**
	 * @throws \Nomad\Exception\Service
	 * @throws \Exception
	 * @return $this
	 */
	public function connect()
	{
		if (!class_exists('Memcache')) {
			throw new \Exception("Memcache is not installed.");
		}

		try {
			$this->_service = new \Memcache();
			$this->_service->connect(
				$this->_host,
				$this->_port
			);
			$this->_isConnected = true;
		}
		catch (\Exception $e) {
			throw new Exception\Service("Unable to connect to Memcache.");
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	public function disconnect()
	{
		if ($this->_isValidService()) {
			$this->_service->close();
			$this->_isConntected = false;
		}

		return $this;
	}

	/**
	 * @param      $key
	 * @param null $default
	 * @return null
	 * @throws \Nomad\Exception\Service
	 */
	public function get($key, $default = null)
	{
		if (!$this->_isValidService()) {
			throw new Exception\Service("Not connected to memcached.");
		}
		$value = $this->_service->get($key, MEMCACHE_COMPRESSED);
		if ($value) {
			return $value;
		}

		return $default;
	}

	/**
	 * @param     $key
	 * @param     $value
	 * @param int $duration
	 * @return $this
	 * @throws \Nomad\Exception\Service
	 */
	public function set($key, $value, $duration = self::_DEFAULT_DURATION)
	{
		if (!$this->_isValidService()) {
			throw new Exception\Service('Not connected to memcache.');
		}
		$this->_service->set($key, $value, MEMCACHE_COMPRESSED, $duration);

		return $this;
	}

	/**
	 * @param $property
	 * @param $value
	 * @return mixed|void
	 */
	public function __set($property, $value)
	{
		$this->set($property, $value);
	}

	/**
	 * @param $property
	 * @return null
	 */
	public function __get($property)
	{
		return $this->get($property);
	}

	/**
	 * @param $key
	 * @return $this
	 * @throws \Nomad\Exception\Service
	 */
	protected function erase($key)
	{
		if (!$this->_isValidService()) {
			throw new Exception\Service('Not connected to memcache.');
		}
		$this->_service->delete($key);

		return $this;
	}
}