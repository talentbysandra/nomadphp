<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Cache\Driver;

use Nomad\Cache as Cache;
use Nomad\Exception as Exception;

/**
 * Class File
 *
 * @package Nomad\Cache\Driver
 * @author  Mark Hillebert
 */
class File
	extends Cache\Driver
{
	/**
	 * @var int Default duration of cache
	 */
	protected $_defaultDuration = 21600; // 6 hours 60*60*6

	/**
	 * @var string
	 */
	protected $_cacheDir;

	/**
	 * @var string
	 */
	protected $_filePrefix;

	/**
	 * @var string
	 */
	protected $_extention = '.nomad';

	/**
	 * @param array $params
	 * @throws Exception\Service
	 */
	public function __construct($params = array())
	{
		if (!isset($params['cacheDir'])) {
			throw new Exception\Service('You must pass in `cacheDir` to use file based cache.');
		}
		$this->_cacheDir = $params['cacheDir'];
	}

	/**
	 * Changes the duration on future sets (not existing);
	 *
	 * @param $seconds
	 * @return $this
	 */
	public function setDuration($seconds)
	{
		$this->_defaultDuration = $seconds;

		return $this;
	}

	/**
	 * Change the default prefix for cache filename
	 *
	 * @param $prefix
	 * @return $this
	 */
	public function setFilePrefix($prefix)
	{
		$this->_filePrefix = $prefix;

		return $this;
	}

	/**
	 * @param     $key
	 * @param     $value
	 * @param int $duration
	 * @return $this
	 * @throws \Nomad\Exception\Service
	 */
	public function set($key, $value, $duration = null)
	{
		if (!file_exists($this->_cacheDir)) {
			mkdir($this->_cacheDir);
		}
		$time = time();
		if (!$duration) {
			$duration = $this->_defaultDuration + $time;
		}
		else {
			$duration += $time;
		}
		$cacheFilename = $this->_cacheDir . DIRECTORY_SEPARATOR . $this->_filePrefix . sha1($key . date_default_timezone_get()) . $this->_extention;
		file_put_contents($cacheFilename, $duration . "\n" . gzcompress(serialize($value)));

		return $this;
	}

	/**
	 * Shortcut to set into cache using default duration
	 *
	 * @param $property
	 * @param $value
	 * @return mixed|void
	 */
	public function __set($property, $value)
	{
		$this->set($property, $value);
	}

	/**
	 * @param      $key
	 * @param null $default
	 * @return null
	 * @throws \Nomad\Exception\Service
	 */
	public function get($key, $default = null)
	{
		$time          = time();
		$cacheFilename = $this->_cacheDir . DIRECTORY_SEPARATOR . $this->_filePrefix . sha1($key . date_default_timezone_get()) . $this->_extention;
		if (file_exists($cacheFilename)) {
			//check first line for expiration
			$fileHandle = fopen($cacheFilename, 'r');
			$timeLine   = trim(fgets($fileHandle));
			fclose($fileHandle);
			if ($time < $timeLine) {
				$data = substr(file_get_contents($cacheFilename), 11);

				return unserialize(gzuncompress($data));
			}
			else {
				unlink($cacheFilename);
			}
		}

		return $default;
	}

	/**
	 * @param $property
	 * @return null
	 */
	public function __get($property)
	{
		return $this->get($property);
	}

	/**
	 * @param $id
	 * @throws \Nomad\Exception\Service
	 * @return $this
	 */
	public function expire($id)
	{
		if (file_exists($this->_cacheDir . DIRECTORY_SEPARATOR . $this->_filePrefix . sha1($id . date_default_timezone_get()) . $this->_extention)) {
			unlink($this->_cacheDir . DIRECTORY_SEPARATOR . $this->_filePrefix . sha1($id . date_default_timezone_get()) . $this->_extention);
		}

		return $this;
	}
}