<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Validator;
/**
 * Class EmailAddress
 *
 * @package Nomad\Validator
 * @author  Mark Hillebert
 */
class EmailAddress
	extends \Nomad\Validator\AbstractValidator
{
	/**
	 * @var string Message to display when validation fails
	 */
	protected $_message = 'Invalid email address.';

	/**
	 * checks for validity.
	 *
	 * @param string $value
	 * @param array  $formValues
	 * @return bool
	 */
	public function isValid($value, $formValues = array())
	{
		$atPosition = strrpos($value, "@");
		if (is_bool($atPosition) && !$atPosition) {
			return false;
		}
		else {
			$domain = substr($value, $atPosition + 1);
			if (!(checkdnsrr($domain, "MX") || checkdnsrr($domain, "A"))) { // domain not found in DNS
				return false;
			}

			$local        = substr($value, 0, $atPosition);
			$localLength  = strlen($local);
			$domainLength = strlen($domain);

			if ($localLength < 1 || $localLength > 64) { // local part length exceeded
				return false;
			}
			elseif ($domainLength < 1 || $domainLength > 255) { // domain part length exceeded
				return false;
			}
			elseif ($local[0] == '.' || $local[$localLength - 1] == '.') { // local part starts or ends with '.'
				return false;
			}
			elseif (preg_match('/\\.\\./', $local)) { // local part has two consecutive dots
				return false;
			}
			elseif (preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain) === false) { // character not valid in domain part
				return false;
			}
			elseif (preg_match('/\\.\\./', $domain)) { // domain part has two consecutive dots
				return false;
			}
			elseif (preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\", "", $local)) === false) {
				/** character not valid in local part unless
				 * local part is quoted
				 */
				if (preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\", "", $local)) === false) {
					return false;
				}
			}
		}

		return true;
	}
}