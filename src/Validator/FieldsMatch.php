<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Validator;

use Nomad\Exception as Exception;

/**
 * Class FieldsMatch
 *
 * @package Nomad\Validator
 * @author  Mark Hillebert
 */
class FieldsMatch
	extends AbstractValidator
{
	/**
	 * @var array
	 */
	protected $_fieldsThatMustMatch;

	/**
	 * @var string Message to display when validation fails
	 */
	protected $_message = "Fields do not match.";

	/**
	 * @param array $params
	 * @throws \Nomad\Exception\Form
	 */
	public function __construct(array $params)
	{
		if (!isset($params['fieldNames'])) {
			throw new Exception\Form('FieldMatch validator "fieldNames" must be passed an array of fields that must match.');
		}

		$this->_fieldsThatMustMatch = $params['fieldNames'];

		parent::__construct($params);
	}

	/**
	 * checks for validity.
	 *
	 * @param string $value
	 * @param array  $formValues
	 * @internal param array|null $params
	 * @return bool
	 */
	public function isValid($value, $formValues = array())
	{

		foreach ($this->_fieldsThatMustMatch as $fieldName) {
			if (!isset($formValues[$fieldName])) {
				return false;
			}

			if ($formValues[$fieldName] != $value) {
				return false;
			}
		}

		return true;
	}
}