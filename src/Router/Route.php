<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Router;

use Nomad\Core as Core;
use Nomad\Exception as Exception;

/**
 * Class Route
 *
 * @package Nomad\Router
 * @author  Mark Hillebert
 */
class Route
	extends Core\BaseClass
{
	/**
	 * @const string Header to send browser
	 */
	const RESPONSE_401 = 'HTTP/1.0 403 Unauthorized';
	const RESPONSE_403 = 'HTTP/1.0 403 Forbidden';
	const RESPONSE_404 = 'HTTP/1.0 404 Not Found';

	/**
	 * @var string Regex pattern
	 */
	protected $_pattern;

	/**
	 * @var string Full namespace of route
	 */
	protected $_namespace;

	/**
	 * @var string Action/Method to execute
	 */
	protected $_action;

	/**
	 * @var array parameters for action
	 */
	protected $_params = array();

	/**
	 * @param array $options
	 */
	public function __construct($options = array())
	{
		if (is_object($options)) {
			$options = get_object_vars($options);
		}
		$options['inspector'] = false;
		parent::__construct($options);
	}
}