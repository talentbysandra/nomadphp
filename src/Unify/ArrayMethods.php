<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Unify;
/**
 * Class ArrayMethods
 *
 * @package Nomad\Unify
 * @author  Mark Hillebert
 */
class ArrayMethods
{
	/**
	 * Removes any 'empty' items from an array
	 *
	 * @param $array
	 * @return array
	 */
	public static function clean($array)
	{
		if (is_array($array)) {
			return array_filter(
				$array,
				function ($item) {
					return !empty($item);
				});
		}
	}

	/**
	 * Removes white space from each item in an array
	 *
	 * @param $array
	 * @return array
	 */
	public static function trim($array)
	{
		if (is_array($array)) {
			return array_map(
				function ($item) {
					return trim($item);
				},
				$array);
		}
	}

	/**
	 * Convert an array (and nested arrays) into object form.
	 *
	 * @param $array
	 * @return \stdClass
	 */
	public static function toObject($array)
	{
		$result = new \stdClass();
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				$result->{$key} = self::toObject($value);
			}
			else {
				$result->{$key} = $value;
			}
		}

		return $result;
	}

	/**
	 * Flattens a multi-dimensional array to a single dimension
	 *
	 * @param       $array
	 * @param array $return
	 * @return array
	 */
	public static function flatten($array, $return = array())
	{
		foreach ($array as $key => $value) {
			if (is_array($value) || is_object($value)) {
				$return = self::flatten($value, $return);
			}
			else {
				$return[] = $value;
			}
		}

		return $return;
	}

	/**
	 * Checks for array intersection. Returns TRUE if at least one intersection occurs.
	 * Note, this handles 1 dimensional arrays (unless checking for arrays as elements)
	 *
	 * @param $array1
	 * @param $array2
	 * @return bool
	 */
	public static function hasIntersection($array1, $array2)
	{
		return count(array_intersect($array1, $array2)) > 0 ? true : false;
	}

	/**
	 * Returns true if an array has at least one index as a string
	 *
	 * @param $array
	 * @return bool
	 */
	public static function isAssoc($array)
	{
		return (bool)count(array_filter(array_keys($array), 'is_string'));
	}
}