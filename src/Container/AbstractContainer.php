<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container;

use Nomad\Exception\Container;

/**
 * Class AbstractContainer
 *
 * @package Nomad\Container
 * @author  Mark Hillebert
 */
abstract class AbstractContainer
{
	/**
	 * @var string
	 */
	public $identifier;

	/**
	 * @var array
	 */
	protected $_children = array();

	/**
	 * @var self
	 */
	protected $_parent;

	/**
	 * @var array
	 */
	protected $_options;

	/**
	 * @var \Nomad\Container\AccessControl\Aco
	 */
	//protected $_aco;

	public function __construct($options = array())
	{
		foreach ($options as $property => $value) {
			// move any options into respective property
			if (\property_exists($this, "_" . $property)) {
				$this->{"_" . $property} = $value;
				unset($options[$property]);
			}
			if (\property_exists($this, $property)) {
				$this->$property = $value;
				unset($options[$property]);
			}
		}

		//set remaining options into options array
		$this->_options = $options;
	}

	/**
	 * @return array
	 */
	public function getOptions()
	{
		return $this->_options;
	}

	/**
	 * @param AbstractContainer $childContainer
	 */
	public function appendChild(AbstractContainer $childContainer)
	{
		$childContainer->_parent =  &$this;
		if (isset($childContainer->identifier)) {
			$this->_children[$childContainer->identifier] = $childContainer;
		}
		else {
			$this->_children[] = $childContainer;
		}
	}

	/**
	 * @param AbstractContainer $childContainer
	 */
	public function prependChild(AbstractContainer $childContainer)
	{
		$childContainer->_parent = $this->identifier;
		array_unshift($this->_children, $childContainer);
	}

	/**
	 * @param $childName
	 */
	public function removeChild($childName)
	{
		if (isset($this->_children[$childName])) {
			unset($this->_children[$childName]);
		}
	}

	/**
	 * @return array|\Nomad\Container\AbstractContainer
	 */
	public function getChildren()
	{
		return $this->_children;
	}

	/**
	 * @return AbstractContainer
	 */
	public function getParent()
	{
		return $this->_parent;
	}

	/**
	 * Returns the child within the children array. Does not search children for children: use findChild()
	 *
	 * @param $identifier
	 * @return null
	 */
	public function getChild($identifier)
	{
		if (isset($this->_children[$identifier])) {
			return $this->_children[$identifier];
		}

		return null;
	}

	/**
	 * Finds all the children and grandchildren if container with identifier
	 *
	 * @param $identifier
	 * @return array|\Nomad\Container\AbstractContainer
	 */
	public function findChildren($identifier)
	{
		$result = $this->_searchForChild($identifier, $this);
		if (count($result) == 1) {
			return $result[0];
		}

		return $result;
	}

	/**
	 * Locates a child in the container by identifier. If more than one exist, throw exception.
	 *
	 * @param $identifier
	 * @return mixed
	 * @throws \Nomad\Exception\Container
	 */
	public function findChild($identifier)
	{
		$result      = $this->_searchForChild($identifier, $this);
		$resultCount = count($result);
		if ($resultCount > 1) {
			throw new Container("Multiple children of identifier '{$identifier}' exist.");
		}

		if ($resultCount == 1) {
			$result = $result[0];
		}

		return $result;
	}

	/**
	 * @param       $identifier
	 * @param       $node
	 * @param array $result
	 * @return array
	 */
	protected function _searchForChild($identifier, $node, $result = array())
	{

		$children = $node->getChildren();
		foreach ($children as $subChild) {
			$subResult = $subChild->findChild($identifier);
			if (!empty($subResult)) {
				$result[] = $subResult;
			}
		}

		if ($node->identifier == $identifier) {

			$result[] = $node;
		}

		return $result;
	}

	/**
	 * Does this have children
	 *
	 * @return bool
	 */
	public function hasChildren()
	{
		return count($this->_children) > 0;
	}
}