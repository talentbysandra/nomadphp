<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container\Html;

use Nomad\Core\Session;
use Nomad\Exception as Exception;

/**
 * Class Navigation
 *
 * @package Nomad\Container\Html
 * @author  Mark Hillebert
 */
class Navigation
	extends Generic
{
	/**
	 * @const string Default navigation wrapper
	 */
	const DEFAULT_ROOT_TAG = 'nav';
	const DEFAULT_LIST_TYPE_TAG = 'ul';

	/**
	 * @param array $options
	 * @throws Exception\Container
	 */
	public function __construct($options = array())
	{
		$this->_tag = isset($options['wrapperTag']) ? $options['Wrapper'] : self::DEFAULT_ROOT_TAG;
		$tag     = isset($options['tag']) ? $options['tag'] : self::DEFAULT_LIST_TYPE_TAG;
		$options = array_merge($options, ['identifier' => 'main-section', 'tag' => $tag]);
		$this->appendChild(new \Nomad\Container\Html\Generic($options));
	}

	/**
	 * Add link to menu
	 *
	 * @param array $options
	 * @throws \Nomad\Exception\Container
	 * @return $this
	 */
	public function addLink($options = array())
	{
		if (!isset($options['label'])) {
			throw new Exception\Container("You must pass in a label in the options array.");
		}

		if (isset(Session::getSession()->aco) && isset($options['role'])) {
			Session::getSession()->aco->addResource($options['label'], $options['role']);
			if (!Session::getSession()->aco->isPermitted($options['label'], $options['role'])) {
				return $this;
			}
		}

		if (!isset($options['href']) || !isset($options['label'])) {
			throw new Exception\Container('You must pass both a href and label');
		}

		$li   = new Generic(array_merge(array('tag' => 'li'), $options));
		$text = $options['label'];
		$href = $options['href'];
		unset($options['href'], $options['label']);
		$li->appendChild(
			new Generic(
				array(
					'tag'        => 'a',
					'attributes' => array(
						'href' => $href,
					),
					'text'       => $text
				)));
		$this->findChild('main-section')->appendChild($li);

		return $this;
	}

	/**
	 * Add sub-menu to existing navigation item
	 *
	 * @param       $identifier
	 * @param array $options
	 * @return $this
	 * @throws \Nomad\Exception\Container
	 */
	public function addSubMenuTo($identifier, $options = array())
	{

		if (!isset($options['menu'])) {
			throw new Exception\Container('addSubMenu expects `menu` as a key.');
		}

		$ul = new Generic(array('tag' => 'ul'));
		if (isset($options['ulClass'])) {
			$ul->addClass($options['ulClass']);
		}

		foreach ($options['menu'] as $item) {
			$li = new Generic(array_merge($item, array('tag' => 'li')));

			if (!isset($item['href'])) {//just a label
				$li->appendChild(
					new Generic(
						array(
							'tag'  => 'label',
							'text' => $item['label']
						)
					));
			}
			else {

				$a = new Generic(
					array(
						'tag'        => 'a',
						'attributes' => array('href' => $item['href']),
						'text'       => $item['label'],

					));
				if (isset($options['aClass'])) {
					$a->addClass($options['aClass']);
				}

				$li->appendChild($a);
			}
			$ul->appendChild($li);
		};
		$children = $this->findChildren($identifier);

		if (is_array($children)) {
			foreach ($children as $child) {
				if (isset($options['liClass'])) {
					$child->addClass($options['liClass']);
				}

				$subChildren = $child->getChildren();
				if (isset($options['aClass']) && isset($subChildren[0])) {
					$subChildren[0]->addClass($options['aClass']);
					$subChildren[0]->appendChild(
						new Generic(
							array(
								'tag'        => 'b',
								'attributes' => array('class' => 'caret')
							)));
				}

				$child->appendChild($ul);
			}
		}
		else {
			if (isset($options['liClass'])) {
				$children->addClass($options['liClass']);
			}

			$subChildren = $children->getChildren();
			if (isset($options['aClass']) && isset($subChildren[0])) {
				$subChildren[0]->addClass($options['aClass']);
				$subChildren[0]->addAttribute('data-toggle', 'dropdown');
				$subChildren[0]->appendChild(
					new Generic(
						array(
							'tag'        => 'b',
							'attributes' => array('class' => 'caret')
						)));
			}

			$children->appendChild($ul);
		}

		return $this;
	}
}