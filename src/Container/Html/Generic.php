<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container\Html;

/**
 * Class Generic
 *
 * @package Nomad\Container\Html
 * @author  Mark Hillebert
 */
class Generic
	extends AbstractHtmlContainer
{
	/**
	 * @var string
	 */
	protected $_text = '';

	/**
	 * @param $tag
	 * @return $this
	 */
	public function setTag($tag)
	{
		$this->_tag = $tag;

		return $this;
	}
}