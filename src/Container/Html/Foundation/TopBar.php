<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container\Html\Foundation;

use Nomad\Container\Html\FoundationNavigation;
use Nomad\Container\Html\Generic;

/**
 * Class TopBar
 * This class is messy and can be done much better
 *
 * @package Nomad\Container\Html\Foundation
 * @author  Mark Hillebert
 */
class TopBar
	extends FoundationNavigation
{
	/**
	 * @var array
	 */
	protected $_attributes = array(
//        'class'=> 'top-bar',
'data-topbar' => true,
	);

	/**
	 * @param array $options
	 */
	public function __construct($options = array())
	{
		$options['tag'] = 'ul';
		if (isset($options['title'])) {
			$title = new Generic(
				array(
					'identifier' => 'title-area',
					'tag'        => 'ul'
				));
			$title->addClass('title-area');
			$li = new Generic(array('tag' => 'li'));
			$li->addClass('name');
			$h1         = new Generic(array('tag' => 'h1'));
			$titleArray = array(
				'tag'  => isset($options['title-link']) ? 'a' : 'span',
				'text' => $options['title'],
			);

			if (isset($options['title-link'])) {
				$titleArray['attributes'] = array('href' => $options['title-link']);
			}

			$h1->appendChild(new Generic($titleArray));
			$li->appendChild($h1);
			$title->appendChild($li);
			parent::appendChild($title);
		}

		$section = new Generic(
			array(
				'identifier' => 'main-section',
				'tag'        => 'div',
				//            'attributes' => array(
				//                'class' => 'top-bar-section'
				//            )
			));
		if (isset($options['class'])) {
			$section->addClass($options['class']);
		}
		$firstUl = new Generic(array('tag' => 'ul'));
		$section->appendChild($firstUl);
		parent::appendChild($section);

		parent::__construct($options);
	}

	/**
	 * @param       $identifier
	 * @param array $options
	 * @return $this|void
	 * @throws \Nomad\Exception\Container
	 */
	public function addSubMenuTo($identifier, $options = array())
	{
		parent::addSubMenuTo(
			$identifier, array_merge(
			$options, array(
			'liClass' => 'has-dropdown',
			'ulClass' => 'dropdown'
		)));
	}
}