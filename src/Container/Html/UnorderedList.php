<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container\Html;

/**
 * Class UnorderedList
 *
 * @package Nomad\Container\Html
 * @author  Mark Hillebert
 */
class UnorderedList
	extends Generic
{
	/**
	 * @param array $options
	 * @throws \Nomad\Exception\Container
	 */
	public function __construct($options = array())
	{
		$options['tag'] = 'ul';
		parent::__construct($options);
	}

	/**
	 * @param \Nomad\Container\AbstractContainer|array $options
	 * @return $this|void
	 */
	public function appendChild($options)
	{
		if (is_object($options) && get_parent_class($options) == 'Nomad\Container\Html\AbstractHtmlContainer') {
			$li = $options;
		}
		else {
			$options['tag'] = 'li';
			$li             = new Generic($options);
		}

		parent::appendChild($li);

		return $this;
	}
}