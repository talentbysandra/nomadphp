<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Container\Html;

use Nomad\Container\AbstractContainer;
use Nomad\Core\Registry;
use Nomad\Exception\Container;

/**
 * Class AbstractHtmlContainer
 *
 * @package Nomad\Container\Html
 * @author  Mark Hillebert
 */
abstract class AbstractHtmlContainer
	extends AbstractContainer
{
	/**
	 * @var string Html Tag of element
	 */
	protected $_tag;

	/**
	 * @var array Associative array of attributes and values
	 */
	protected $_attributes = array();

	/**
	 * @var
	 */
	protected $_divider;

	/**
	 * @param array $options
	 * @throws \Nomad\Exception\Container
	 */
	public function __construct($options = array())
	{
		if (!isset($options['tag'])) {
			throw new Container('You must pass in a tag option.');
		}

		parent::__construct($options);
	}

	/**
	 * Helper function to render the attributes of the element
	 *
	 * @return string
	 */
	protected function _renderAttributes()
	{
		$attributeString = '';
		foreach ($this->_attributes as $attribute => $value) {
			switch (true) {
				case is_array($value):
					$attributeString .= $attribute . "='" . implode(' ', $value) . "' ";
					break;
				case is_string($value):
					$attributeString .= " {$attribute}='{$value}' ";
					break;
				case is_bool($value):
					$attributeString .= "{$attribute} ";
					break;
			}
		}

		return empty($attributeString) ? "" : " " . trim($attributeString);
	}

	/**
	 * Renders the element
	 *
	 * @return string
	 */
	public function render()
	{
		$htmlString  = $this->_text;
		$activeClass = '';
		foreach ($this->_children as $child) {
			$htmlString .= $child->render();
			if (isset($child->_attributes['href']) && $child->_attributes['href'] == Registry::get('router')
																							 ->getCleanUrl()
			) {
				if (isset($this->_attributes['class'])) {
					$activeClass = " class='active " . $this->_attributes['class'] . "'";
				}else{

					$activeClass = " class='active'";
				}
			}
		}

		if ($this->_tag) {
			$return = "<{$this->_tag}{$activeClass}" . $this->_renderAttributes() . ">{$htmlString}</{$this->_tag}>";
		}
		else {
			$return = $htmlString;
		}

		return $return;
	}

	/**
	 * Sets the id attribute for the element
	 *
	 * @param $id
	 */
	public function setId($id)
	{
		$this->_attributes['id'] = $id;
	}

	/**
	 * Gets the id attribute of the element if exists
	 *
	 * @return null
	 */
	public function getId()
	{
		if (isset($this->_attributes['id'])) {
			return $this->_attributes['id'];
		}

		return null;
	}

	/**
	 * Adds a class to the element
	 *
	 * @param $className
	 * @return null
	 */
	public function addClass($className)
	{
		if (!isset($this->_attributes['class'])) {
			$this->_attributes['class'] = array();
		}

		if (is_string($this->_attributes['class'])) {
			$this->_attributes['class'] = array(
				$this->_attributes['class'],
				$className
			);
		}
		elseif (!isset($this->_attributes['class'][$className])) {
			$this->_attributes['class'][] = $className;
		}
	}

	/**
	 * Removes a class from element if exists
	 *
	 * @param $className
	 */
	public function removeClass($className)
	{
		if (isset($this->_attributes['class'][$className])) {
			unset($this->_attributes['class'][$className]);
		}
	}

	/**
	 * Determines if class exists on element
	 *
	 * @param $className
	 * @return bool
	 */
	public function hasClass($className)
	{
		return isset($this->_attributes['class'][$className]);
	}

	public function setDivider(Generic $divider)
	{
		$this->_divider = $divider;
	}

	/**
	 * Add attribute to element
	 *
	 * @param $attributeName
	 * @param $value
	 */
	public function addAttribute($attributeName, $value)
	{
		$this->_attributes[$attributeName][] = $value;
	}

	/**
	 * @param                                                   $identifier
	 * @param array|\Nomad\Container\Html\AbstractHtmlContainer $options
	 * @return $this
	 * @throws \Nomad\Exception\Container
	 */
	public function appendToChild($identifier, $options = array())
	{
		if (!isset($this->_children[$identifier])) {
			throw new Container("Identifier not found among the children.");
		}

		if (is_object($options) && get_parent_class($options) == 'Nomad\Container\Html\AbstractHtmlContainer') {
			$child = $options;
		}
		else {
			$child = new Generic($options);
		}

		$this->_children[$identifier]->appendChild($child);

		return $this;
	}
}