<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad;
/**
 * Class Application
 *
 * @package Nomad
 * @author  Mark Hillebert
 */
class Application
{
	/**
	 * Constructor
	 */
	function __construct()
	{
		defined('ENVIRONMENT') or define('ENVIRONMENT', isset($_SERVER['ENVIRONMENT']) ? $_SERVER['ENVIRONMENT'] : 'production');
	}

	/**
	 * @throws Exception\Autoloader
	 */
	function nomadAutoLoader()
	{
		\Nomad\Core\Autoloader::setIncludePaths(VENDOR_ROOT, APPLICATION_ROOT, APPLICATION_ROOT . DIRECTORY_SEPARATOR . PACKAGE_DIRECTORY);
		\Nomad\Core\Autoloader::register();
	}

	/**
	 * same as run() in other projects
	 */
	function nomadApplicationExecutor()
	{
		$this->nomadAutoLoader();
		//DO NOT DISPLAY ERRORS TO USER
		if (ENVIRONMENT !== 'development') {

			ini_set("display_errors", 0);
			ini_set("log_errors", 1);

			//Define where do you want the log to go, syslog or a file of your liking with
			ini_set("error_log", APPLICATION_ROOT . '/php_fatal_errors.log');

			register_shutdown_function(
				function () {
					$last_error = error_get_last();
					if (!empty($last_error) &&
						in_array($last_error['type'], [E_ERROR, E_COMPILE_ERROR, E_PARSE, E_CORE_ERROR, E_USER_ERROR])
					) {
						$router     = \Nomad\Core\Registry::get('router');
						$route      = $router->findRoute('500');
						$dispatcher = new \Nomad\Core\Dispatcher();
						$dispatched = $dispatcher->dispatch($route);
						(new \Nomad\Core\Renderer($dispatched))->render();
					}
				});
		}

		if (file_exists(APPLICATION_ROOT . '/Bootstrap.php')) {
			$bootstrap = 'Bootstrap';
		}
		else {
			$bootstrap = 'Nomad\Core\Bootstrap';
		}
		(new $bootstrap())->invoke();
		$dispatched = \Nomad\Core\Registry::get('dispatcher')->dispatch();
		while (!$dispatched) { //handles any redirects
			\Nomad\Core\Registry::get('dispatcher')->unStop();
			$dispatched = \Nomad\Core\Registry::get('dispatcher')->dispatch();
		}

		if (\Nomad\Core\Registry::get('dispatcher')->getContinueToRenderer() === true) {
			(new \Nomad\Core\Renderer($dispatched))->render();
		}
		else {
		}

		// delete any uploads at the end of the script
		if (\Nomad\Core\Session::getSession()->FILES) {
			// delete all -- 1 in 10 chance: Help minimize pile up
			$draw = rand(0, 10);
			if ($draw == 1) {
				array_map("unlink", glob(TEMP_DIR . '/*'));
			}
			else {
				foreach (\Nomad\Core\Session::getSession()->FILES as $file) {
					unlink($file['tmp_name']);
				}
			}
		}
	}
}






