<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Filter;
/**
 * Interfacer Validator_Interface
 *
 * @package Nomad\Validator
 * @author  Mark Hillebert
 */
interface Filter_Interface
{
	/**
	 * @param       $value
	 * @param array $formValues
	 * @return mixed
	 */
	public function filter($value, $formValues = array());
}
