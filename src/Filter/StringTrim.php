<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Filter;
/**
 * Class StringTrim
 * simply trims string
 *
 * @package Nomad\Validator
 * @author  Mark Hillebert
 */
class StringTrim
	extends AbstractFilter
{
	/**
	 * Filters valid html tags and no events
	 *
	 * @param       $value
	 * @param array $formValues
	 * @return bool|mixed
	 */
	public function filter($value, $formValues = array())
	{
		return trim($value);
	}
}