<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;

use Nomad\Exception\Argument;
use Nomad\Exception\Render;
use Nomad\Unify\StringMethods;

/**
 * Class View
 * A renderable class
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class View
	extends BaseClass
{
	/**
	 * @var string Page's title
	 */
	protected $_title;

	/**
	 * @var string Path and file of View
	 */
	protected $_file;

	/**
	 * @var View
	 */
	protected $_layout;

	/**
	 * The name of the theme.
	 * Usually not used by view, but available to override default.
	 *
	 * @var string
	 */
	protected $_theme;

	/**
	 * Variable data for the view phtml file
	 *
	 * @var array
	 */
	protected $_data = array();

	/**
	 * Reflected? class method that called the method
	 *
	 * @var string
	 */
	protected $_caller;

	/**
	 * Javascript files
	 *
	 * @var array
	 */
	protected $_scripts = array();

	/**
	 * Css files
	 *
	 * @var array
	 */
	protected $_css = array();

	/**
	 * Determines whether to cache this render
	 *
	 * @var bool
	 */
	protected $_isCachable = false;

	/**
	 * Used to differentiate different cache files for same view
	 *
	 * @var string
	 */
	protected $_cacheIdentifier = 'view';

	/**
	 * @param array $options
	 */
	public function __construct($options = array())
	{

		$options['inspector'] = false;
		if (isset($options['view'])) {
			$this->setViewPath($options['view']);
			unset($options['view']);
		}
		//check to see if a namespace and action were passed - used to create a layout on View's creation.
		if (isset($options['layout'])) {
			$this->setLayout($options['layout']);
			unset($options['layout']);
		}

		parent::__construct($options);
	}

	/**
	 * Return the calling class?
	 *
	 * @return mixed
	 */
	public function getCaller()
	{
		return $this->_caller;
	}

	/**
	 * Renders the View
	 *
	 * @TODO maybe render the view inside its layout then within the theme. Currently ignores layout
	 * @return string
	 */
	public function render()
	{

		if ($this->_isCachable) {
			/**
			 * 1. get or make cache
			 * 2. find correct file
			 */
			$cacher    = $this->_getCache();
			$fromCache = $cacher->get($this->_cacheIdentifier);
			if ($fromCache) {
				return $fromCache;
			}
		}
		//not in cache or not cachable

		//render any data that are View objects
		foreach ($this->_data as $name => $value) {
			if (is_object($value) && $value instanceof \Nomad\Core\View) {
				$this->_data[$name] = $value->render();
				$this->_css         = array_merge($value->getCss(), $this->_css);
				$this->_scripts     = array_merge($value->getScripts(), $this->_scripts);
				if ($value->getTitle()) {
					$this->_title = $value->getTitle();
				}
			}
		}

		$this->_checkNamespacedFile();
		ob_start();
		include($this->_file);
		$content = ob_get_contents();
		ob_end_clean();

		//check for a layout and pass the content into it as 'content', then render and return it's render
		if ($this->_layout) {
			if (isset($this->_data['content'])) {
				unset ($this->_data['content']);
				$this->_layout->setData($this->_data);
			}
			$this->_layout->content = $content; //TEST: PASS THE DATA DIRECTLY TO THE LAYOUT
			$content                = $this->_layout->render();
		}

		if ($this->_isCachable) {
			$cacher = $this->_getCache();
			$cacher->set($this->_cacheIdentifier, $content);
		}

		return $content;
	}

	/**
	 * Helper function for themes:
	 * Helps convert namespaced Themes e.g. NFL\Generic (passed in with setTheme('NFL\\Generic')
	 * to map to the proper folder: theme\NFL\Generic\layout\nba-generic.phtml
	 *
	 * Changes the $this->_file property
	 */
	protected function _checkNamespacedFile()
	{
		if (strpos($this->_file, '\\') !== false) {
			$this->_file = preg_replace('~\\\~', '/', $this->_file, 1);
			$this->_file = preg_replace('~\\\~', '-', $this->_file, 1);
		}
	}

	/**
	 * Magic method to set data for view file
	 *
	 * @param $name
	 * @param $value
	 * @return mixed|void
	 */
	public function __set($name, $value)
	{
		$this->_data[$name] = $value;
	}

	/**
	 * Magic method to retrieve data for view file
	 *
	 * @param $name
	 * @return mixed|null
	 */
	public function __get($name)
	{
		return array_key_exists($name, $this->_data) ? $this->_data[$name] : null;
	}

	/**
	 * Setter for special title property
	 *
	 * @param $pageTitle
	 */
	public function setTitle($pageTitle)
	{
		$this->_title = $pageTitle;
	}

	/**
	 * Getter for special title property
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->_title;
	}

	/**
	 * Magic method for getting/setting data into the view
	 *
	 * @param $name
	 * @param $value
	 * @return $this|null|void
	 */
	public function __call($name, $value)
	{
		try {
			parent::__call($name, $value);
		}
		catch (\Exception $e) {
			//remove 'set' from variable name;
			if (strstr($name, 'set')) {
				$name = substr($name, 3);
			}
			$this->_data[$name] = array_shift($value);
		}
	}

	/**
	 * Sets the path for the view.
	 * Can pass either a full path with forward slashes or an annotated-style string (see Annotation for details)
	 *
	 * @param $fullFilename
	 * @throws \Nomad\Exception\Render
	 */
	public function setViewPath($fullFilename)
	{
		/**
		 * need to check what was passed in:
		 * if there are forward slashes, then a path was give => test the path for existence and go.
		 * if there is double colon :: then explode it and begin path creation, test for existence and go
		 */
		if (strstr($fullFilename, '/')) {
			$file = $fullFilename;
		}
		else {
			$annotatedFileParts = explode('::', $fullFilename);
			if (count($annotatedFileParts) == 1) {
				// only filename was given

				$file = StringMethods::makePath(APPLICATION_ROOT, 'view', $annotatedFileParts[0] . '.phtml');
			}
			else {
				// namespace and filename were given
				$lastPart       = array_pop($annotatedFileParts) . '.phtml';
				$namespaceParts = explode('\\', $annotatedFileParts[0]);
				if (count($namespaceParts) > 0) {
					$firstPart = StringMethods::makePath(APPLICATION_ROOT, 'package');
					if ($namespaceParts[0] == APPLICATION_DIR_NAME) {
						$firstPart = APPLICATION_ROOT . '/../';
					}
					$secondPart =
						StringMethods::makePath(array_shift($namespaceParts), 'view', str_replace('\\', DIRECTORY_SEPARATOR, implode('\\', $namespaceParts)));
					$file       = str_replace('//', '/', StringMethods::makePath($firstPart, $secondPart, $lastPart));
				}
				else {
					$file = StringMethods::makePath($lastPart);
				}
			}
		}

		if (!\file_exists($file)) {
			throw new \Nomad\Exception\Render("View file: '{$file}' does not exist!");
		}

		$this->_file = $file;
	}

	/**
	 * Change the view's layout
	 * if passed a string, will attempt to parse it to match a file and create a view from that.
	 *
	 * @param string|View $layoutView
	 */
	public function setLayout($layoutView)
	{
		if (is_object($layoutView) && get_class($layoutView) == 'Nomad\Core\View') {
			// passed a view; go ahead and set it

			$this->_layout = $layoutView;
		}
		else {
			// passed a string, try to create a layout view
			$this->_layout = $this->_createLayoutView($layoutView);
		}
	}

	/**
	 * getter for layout
	 *
	 * @return View
	 */
	public function getLayout()
	{
		return $this->_layout;
	}

	/**
	 * @param $name
	 * @param $data
	 * @throws Render
	 */
	public function passIntoLayout($name, $data)
	{
		if (!isset($this->_layout)) {
			throw new Render("No layout to set data into.");
		}

		$this->_layout->$name = $data;
	}

	/**
	 * Attempts to create a layout view from a annotated style string (see annotations file for details)
	 *
	 * @param $annotatedStyleString
	 * @return View|null
	 */
	protected function _createLayoutView($annotatedStyleString)
	{

		$annotatedLayoutParts = explode('::', $annotatedStyleString);
		if (count($annotatedLayoutParts) == 1) {
			// only filename was given

			$layoutFilename = StringMethods::makePath(APPLICATION_ROOT, 'layout', $annotatedLayoutParts[0] . '.phtml');
		}
		else {
			// namespace and filename were given

			$lastPart       = StringMethods::makePath('layout', array_pop($annotatedLayoutParts) . '.phtml');
			$namespaceParts = explode('\\', $annotatedLayoutParts[0]);
			if (count($namespaceParts) > 0) {
				$firstPart = StringMethods::makePath(APPLICATION_ROOT, 'package');
				if ($namespaceParts[0] == APPLICATION_DIR_NAME) {
					$firstPart = APPLICATION_ROOT . '/../';
				}
				$secondPart     =
					StringMethods::makePath(array_shift($namespaceParts), 'view', str_replace('\\', DIRECTORY_SEPARATOR, implode('\\', $namespaceParts)));
				$layoutFilename = str_replace('//', '/', StringMethods::makePath($firstPart, $secondPart, $lastPart));
			}
			else {
				$layoutFilename = StringMethods::makePath($lastPart);
			}
		}

		if (file_exists($layoutFilename)) {
			$layoutView = new View(
				array(
					'file' => $layoutFilename
				));

			return $layoutView;
		}

		return null;
	}

	/**
	 * Sets the name of the theme to use
	 *
	 * @param string $themeName
	 */
	public function setTheme($themeName)
	{
		$this->_theme = $themeName;
	}

	/**
	 * Returns the name of the theme from this view.
	 *
	 * @return string
	 */
	public function getTheme()
	{
		return $this->_theme;
	}

	/**
	 * Returns the data property
	 *
	 * @return array
	 */
	public function getData()
	{
		return $this->_data;
	}

	/**
	 * Removes View|Data from array
	 *
	 * @param int|string $key
	 */
	public function removeChildView($key)
	{
		unset($this->_data[$key]);
	}

	/**
	 * Sets the data property
	 *
	 * @param array $newData
	 */
	public function setData(array $newData = array())
	{
		$this->_data = $newData;
	}

	/**
	 * @param       $block Annotated String of Block
	 * @param array $params
	 * @return
	 * @throws Argument
	 */
	public function useHelper($block, $params = array())
	{
		if (strstr($block, '::') === false) {
			throw new Argument('renderBlock expects the first param to be an Annotated string to a block and must contain `::`');
		}

		$blockParts = explode('::', $block);

		if (strpos($blockParts[0], '\\') === false) {
			// The block must be in the same package, so we'll look there
			preg_match("~^(.*?)\\\~", $this->_caller, $package);
			$package = $package[1];
			$class   = $package . '\\Block\\' . $blockParts[0];
			$action  = $blockParts[1];
		}
		else {
			// The full annotated path to the block was passes in ('{package}\\Block\\{blockName}::{blockAction}
			$class  = $blockParts[0];
			$action = $blockParts[1];
		}

		/**
		 * @todo returns null when class is not found. class_exists() does not work. please fix
		 */
		$block = new $class(array_merge(['action' => $action], $params));//run initialize if exists

		$this->_inspector = new Inspector($block);
		$methodMeta       = $this->_inspector->getMethodMeta($action);
		$this->_injector($block, $methodMeta);
		$result = $block->$action($params);
		if (is_subclass_of($block, 'Nomad\\Core\\Viewable')) {
			//ensure were using the correct (expected) view
			$block->getView()->setViewPath("{$class}::{$action}");

			return $block->getView()->render();
		}
		else {
			return $result;
		}
	}

	/**
	 * Injects services when annotated
	 *
	 * @param $instance
	 * @param $meta
	 */
	protected function _injector(&$instance, $meta)
	{
		if (isset($meta[Annotations::$service])) {
			foreach ($meta[Annotations::$service] as $service) {
				if (strpos($service, '=>') !== false) { // allow for naming
					$serviceAnnotationBreakdown = explode('=>', $service);
					$service                    = trim($serviceAnnotationBreakdown[1]);
				}

				$injectedService = new $service();
				/**
				 * Note: Annotation ACL list must match the list of services.
				 * E.g.
				 * if you pass in 2 service:
				 *
				 * @Nomad\Service    Common\Service\Message, Pickplucker\Service\League
				 *                   then you must pass in the acl actions that pair
				 * @Nomad\Acl\Action create, read
				 *                   where create corresponds with the MessageService and read is with LeagueService
				 */
				$aclAction = null;
				if (isset($meta[Annotations::$aclAction]) && !empty($meta[Annotations::$aclAction])) {
					$aclAction = 'ACL_' . strtoupper(array_shift($meta[Annotations::$aclAction]));
					$injectedService->setAclAction(constant(get_class($injectedService) . '::' . $aclAction));
				}
				else {
					$injectedService->setAclAction(null);
				}

				$instance->addService($injectedService);
			}
		}
	}

	/**
	 * Adds a css file to the head section
	 *
	 * @param        $file
	 * @param string $media
	 */
	public function appendCss($file, $media = 'screen')
	{
		$this->_css[$file] = $media;
	}

	/**
	 * Adds a javascript file to the head section
	 *
	 * @param $file
	 */
	public function removeCss($file)
	{
		unset($this->_css[$file]);
	}

	/**
	 * @return array
	 */
	public function getCss()
	{
		return $this->_css;
	}

	/**
	 * renders the html for added css in the order it was added.
	 */
	public function renderCss()
	{
		$html = '';
		foreach ($this->_css as $fileName => $media) {
			$html .= "<link media='{$media}' href='{$fileName}' rel='stylesheet'>";
		}

		return $html;
	}

	/**
	 * Adds a javascript file to the head section
	 *
	 * @param $file
	 */
	public function appendHeadScript($file, $inline = false)
	{
		$this->_scripts['head'][] = $file;
	}

	/**
	 * Adds a javascript file to the body section
	 *
	 * @param $file
	 */
	public function appendBodyScript($file, $inline = false)
	{
		$this->_scripts['body'][] = $file;
	}

	/**
	 * Removes a javascript file from either head or body
	 *
	 * @param $file
	 */
	public function removeScript($file)
	{
		if (isset($this->_scripts['head'])) {
			unset($this->_scripts['head'][$file]);
		}
		if (isset($this->_scripts['body'])) {
			unset($this->_scripts['body'][$file]);
		}
	}

	/**
	 * @return array
	 */
	public function getScripts()
	{
		return $this->_scripts;
	}

	/**
	 * Renders html for any added scripts
	 *
	 * @return string
	 */
	public function renderHeadScripts()
	{
		return $this->_renderScripts('head');
	}

	/**
	 * Renders html for any added scripts
	 *
	 * @return string
	 */
	public function renderBodyScripts()
	{
		return $this->_renderScripts('body');
	}

	/**
	 * @param        $addendum
	 * @param string $separator
	 * @return string Adds to the title
	 */
	public function renderTitle($addendum = null, $separator = "|")
	{
		if ($addendum) {
			$addendum = " {$separator} {$addendum}";
		}

		return "<title>" . trim("{$this->_title}{$addendum}", " {$separator}") . "</title>";
	}

	/**
	 * Sets whether to cache the render. Default = false;
	 *
	 * @param $boolean
	 */
	public function setIsCachable($boolean)
	{
		$this->_isCachable = $boolean;
	}

	/**
	 * Sets the unique identifier for cache file so one may use same view with multiple caches
	 *
	 * @param $identifier
	 */
	public function setCacheIdentifier($identifier)
	{
		$this->_cacheIdentifier = $identifier;
	}

	/**
	 * Helper for rendering scripts
	 *
	 * @param $type
	 * @return string
	 */
	protected function _renderScripts($type)
	{
		$html = '';
		if (isset($this->_scripts[$type])) {
			foreach ($this->_scripts[$type] as $file) {
				if (substr($file, 0, 1) == '<') {
					$html .= $file;
				}else{
					$html .= "<script type='text/javascript' src='{$file}'></script>";
				}
			}
		}

		return $html;
	}

	/**
	 * Gets the proper cache
	 *
	 * @return \Nomad\Cache\Driver\File|null
	 */
	protected function _getCache()
	{
		if (!isset($this->_cache)) {
			$this->_cache = Registry::get('cache');
			if ($this->_cache) {
				if (!$this->_cacheFilePrefix) {
					$fileInfo = pathinfo($this->_file);
					$prefix   = 'nomad-view-' . $fileInfo['filename'];
					$this->_cache->setFilePrefix($prefix);
				}
			}
		}

		return $this->_cache;
	}
}
