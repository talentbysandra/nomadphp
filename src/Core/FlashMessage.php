<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;
/**
 * Class FlashMessage
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class FlashMessage
	extends BaseClass
{
	/**
	 * Adds a info message to the flash messenger
	 *
	 * @param $message
	 */
	public static function addInfoMessage($message)
	{
		$currentMessages                     = Session::getSession()->flashMessages;
		$currentMessages['info'][]           = $message;
		Session::getSession()->flashmessages = $currentMessages;
	}

	/**
	 * Adds a success message to the flash messenger
	 *
	 * @param $message
	 */
	public static function addSuccessMessage($message)
	{
		$currentMessages                     = Session::getSession()->flashMessages;
		$currentMessages['success'][]        = $message;
		Session::getSession()->flashMessages = $currentMessages;
	}

	/**
	 * Adds a success message to the flash messenger
	 *
	 * @param $message
	 */
	public static function addWarningMessage($message)
	{
		$currentMessages                     = Session::getSession()->flashMessages;
		$currentMessages['warning'][]        = $message;
		Session::getSession()->flashMessages = $currentMessages;
	}

	/**
	 * Adds a failure message to the flash messenger
	 *
	 * @param $message
	 */
	public static function addFailureMessage($message)
	{
		$currentMessages                     = Session::getSession()->flashMessages;
		$currentMessages['failure'][]        = $message;
		Session::getSession()->flashMessages = $currentMessages;
	}

	/**
	 * Adds a sticky message to the flash messenger
	 *
	 * @param $message
	 */
	public static function addStickyMessage($message)
	{
		$currentMessages                     = Session::getSession()->flashMessages;
		$currentMessages['sticky'][]         = $message;
		Session::getSession()->flashMessages = $currentMessages;
	}

	/**
	 * Returns an html formatted string of all messages and removes all but the sticky
	 *
	 * @param array $typeClassArray
	 * @return string
	 */
	public static function displayMessages($typeClassArray = array())
	{
		$currentMessages = Session::getSession()->flashMessages;
		$generatedHtml   = '';
		if ($currentMessages) {
			$generatedHtml = '<div class="flash-messages">';
			foreach ($currentMessages as $type => $messages) {
				foreach ($messages as $message) {
					if ($typeClassArray && isset($typeClassArray[$type])) {
						$generatedHtml .= "<div class='$typeClassArray[$type]'>{$message}</div>";
					}
					else {
						$generatedHtml .= "<div class='{$type}'>{$message}</div>";
					}
				}
				if ($type != 'sticky') {
					unset($currentMessages[$type]);
				}
			}
			$generatedHtml .= "</div>";
			Session::getSession()->flashMessages = $currentMessages;
		}

		return $generatedHtml;
	}

	/**
	 * Returns the array of messages(if any)
	 *
	 * @return null
	 */
	public static function getMessages()
	{
		return Session::getSession()->flashMessages;
	}

	/**
	 * are there any messages?
	 *
	 * @return bool
	 */
	public static function hasMessages()
	{
		return !empty(Session::getSession()->flashMessages);
	}
}