<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;

/**
 * Class Viewable
 * Makes any class into a class that has a View and then can be rendered.
 * Useful for making blocks of content.
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Viewable
	extends BaseClass
{
	/**
	 * @var View
	 */
	protected $_view;

	/**
	 * @var \Nomad\Router\Route The route matching this view.
	 */
	protected $_route;

	/**
	 * @var string
	 */
	protected $_action;

	/**
	 * @var bool Determines if this view should be rendered.
	 */
	protected $_willRenderView = true;

	/**
	 * The calling class::method of this
	 *
	 * @var string
	 */
	protected $_caller;

	/**
	 * @param array $options
	 * @throws \Exception
	 */
	public function __construct($options = array())
	{
		parent::__construct($options);
		//set the default view to the expected place for the action then override if specified in annotation
		if (!$this->_route) {
			$namespace = get_class($this);
			if (isset($options['action'])) {
				$action = $options['action'];
			}
			else {
				$action = array_keys($this->_inspector->getClassMethods())[0]; //@todo, make a default function name to check for default view action setup instead of using the first method as default.
			}
		}
		else {
			$namespace = $this->_route->getNamespace();
			$action    = $this->_route->getAction();
		}
//        if ($action == 'initialize') return;
		$methodMeta = $this->_inspector->getMethodMeta($action);

		$viewOptions['view'] = $namespace . '::' . $action;
		if (!empty($methodMeta[Annotations::$view])) {
			$viewOptions['view'] = $methodMeta[Annotations::$view][0];
		}

		//Default layout setup is handled by view unless annotated otherwise by the Full\Namespace::action
		$viewOptions['layout'] = $namespace . '::' . $action;
		if (!empty($methodMeta[Annotations::$layout])) {
			$viewOptions['layout'] = $methodMeta[Annotations::$layout][0];
		}
		$viewOptions['caller'] = isset($this->_route) ? $this->_route->getNamespace() . '::' . $this->_route->getAction() : null;
		try {
			$view       = new View($viewOptions);
			$this->view = $view;
		}
		catch (\Nomad\Exception\Render $e) {
			if (!empty($methodMeta[Annotations::$view]) && in_array('disable', $methodMeta[Annotations::$view])) {
				//if there is a disable view annotation then set view to null and move on
			}
			else {
				//re-throw the exception
				throw new \Nomad\Exception\Render($e);
			}
		}

		if (method_exists($this, 'initialize')) {
			$this->initialize($options);
		}
	}

	/**
	 * Change if the view should be rendered.
	 *
	 * @param $boolean
	 */
	public function willRenderViews($boolean)
	{
		if (is_bool($boolean)) {
			$this->_willRenderView = $boolean;
		}
	}

	/**
	 * Sets the layout for the view
	 *
	 * @param string|View $layoutView
	 */
	public function setLayout($layoutView)
	{
		$this->view->setLayout($layoutView);
	}

	/**
	 * Gets View
	 *
	 * @return View
	 */
	public function getView()
	{
		return $this->_view;
	}

	/**
	 * Sets the view to View object
	 *
	 * @param View $view
	 */
	public function setView(View $view)
	{
		$this->_view = $view;
	}

	/**
	 * disables Layout
	 */
	public function disableLayout()
	{
		$this->_view->setLayout(null);
		$this->_view->null;
		$this->_willRenderView = false;
	}
}