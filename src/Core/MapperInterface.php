<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;

/**
 * Interfacer MapperInterface
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
interface MapperInterface
{
	/**
	 * @param array $dataSourceArray
	 * @return mixed
	 */
	public function mapFromDataSource(array $dataSourceArray);

	/**
	 * @param array $columnNames
	 * @return mixed
	 */
	public function mapToDataSource(array $columnNames);
}