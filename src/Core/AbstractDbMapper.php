<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;

/**
 * Class AbstractDbMapper
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
abstract class AbstractDbMapper
	implements \Nomad\Core\MapperInterface
{
	/**
	 * @var
	 */
	protected $_model;

	/**
	 * Maps columns from a db to a model
	 *
	 * @param array $dataSourceArray
	 * @return mixed
	 */
	public abstract function mapFromDataSource(array $dataSourceArray);

	/**
	 * Maps an array of model properties to a columnName (in db?)
	 *
	 * @param array $columnNames
	 * @return mixed
	 */
	public abstract function mapToDataSource(array $columnNames);

	/**
	 * Takes a json encoded string and places the decoded values into the model
	 *
	 * @param $jsonValue
	 * @throws \Nomad\Exception\Mapper
	 */
	protected function _jsonToModel($jsonValue)
	{
		if (empty($this->_model)) {
			throw new \Nomad\Exception\Mapper('Protected property _model is not set. Can not convert to json.');
		}

		$values = json_decode($jsonValue, true);

		if ($values) {
			foreach ($values as $key => $value) {
				$this->_model->{'set' . $key}($value);
			}
		}
	}

	/**
	 * Creates a json encoded array from properties given
	 *
	 * @param array $modelProperties Properties to get for encoding.
	 * @return array
	 */
	protected function _modelToJson(array $modelProperties)
	{
		$mapped = array();
		foreach ($modelProperties as $property) {
			$mapped[$property] = $this->_model->{'get' . ucfirst($property)}();
		}

		return json_encode($mapped);
	}
}