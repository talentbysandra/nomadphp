<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;

use Nomad\Exception\Route;
use Nomad\Exception\Service;
use Nomad\Router\Route\Simple;
use Nomad\Unify;

/**
 * Class Controller
 * A Viewable/Renderable Controller.
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Controller
	extends Viewable
{
	/**
	 * @var mixed Usually from url
	 */
	protected $_parameters;

	/**
	 * @var array Holds services
	 */
	private $_services;

	/**
	 * @param array $options
	 */
	public function __construct($options = array())
	{
		parent::__construct($options);

		if ($this->_inspector) {
			$meta = $this->_inspector->getClassMeta();
			if (isset($meta[Annotations::$service])) {
				foreach ($meta[Annotations::$service] as $service) {
					//Were not instantiating the service until its called
					$serviceParts                              = explode('\\', $service);
					$this->_services[array_pop($serviceParts)] = $service;
				}
			}
		}
	}

	/**
	 * Redirects and dispatches to a new Namespace::action.
	 * Note, this will push it's View into the new action (if viewable)
	 *
	 * @param      $namespaceClassActionString
	 * @param null $params
	 * @throws \Nomad\Exception\Route
	 */
	public function redirect($namespaceClassActionString, $params = null)
	{
		$oldView = Registry::get('dispatcher')->getMasterView();
		$parts   = explode("::", $namespaceClassActionString);

		if (count($parts) == 1) {
			//assuming this is a registered route name
			$route = Registry::get('router')->findRoute($namespaceClassActionString);
			if (!$route) {
				throw new Route();
			}
		}
		else {
			$route = new Simple(
				array(
					'namespace' => $parts[0],
					'action'    => $parts[1],
					'params'    => $params
				)
			);
		}

		Registry::get('dispatcher')->stop();
		Registry::set(
			'dispatcher', new Dispatcher(
			array(
				'route'   => $route,
				'oldView' => $oldView
			)));
	}

	/**
	 * Same as redirect above, only issues sets the stop flag .
	 *
	 * @param $responseCode
	 */
	public function error($responseCode)
	{
		Registry::get('dispatcher')->stop();
		Registry::set('dispatcher', new Dispatcher(['route' => Registry::get('router')->findRoute($responseCode)]));
	}

	/**
	 * Handles Service calls if call contains _get{someService}Service;
	 *
	 * @param $name
	 * @param $arguments
	 * @return $this|null
	 * @throws \Nomad\Exception\Service
	 */
	public function __call($name, $arguments)
	{
		$getMatches = \Nomad\Unify\StringMethods::match($name, "^_get([a-zA-Z0-9]+)Service$");
		if (count($getMatches) > 0) {
			//add a slash before each capital letter (except first)
			$class = str_replace('\\', '', preg_replace('/(\w+)([A-Z])/U', '\\1\\\\\2', $getMatches[0]));
			if ($this->_services && array_key_exists($class, $this->_services)) {
				if (is_string($this->_services[$class])) {
					$this->_services[$class] = new $this->_services[$class];
				}

				if ($arguments) {
					if (!is_array($arguments[0])) {
						throw new Service('Injection calls with arguments must be passed in as `method`=>`params` array.');
					}

					foreach ($arguments[0] as $property => $value) {
						$this->_services[$class]->$property($value);
					}
				}

				return $this->_services[$class];
			}
			else {
				//asked for a service that was not annotated
				throw new Service("`{$class}Service` has not been injected. Annotation missing.");
			}
		}
		else {
			//not a Service call -- pass to parent
			return parent::__call($name, $arguments);
		}
	}

	/**
	 * Adds a service to the array
	 *
	 * @param $service
	 */
	public function addService($service)
	{
		$class                 = get_class($service);
		$parts                 = explode('\\', $class);
		$key                   = array_pop($parts);
		$this->_services[$key] = $service;
	}
}


