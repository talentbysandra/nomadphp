<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;

use Nomad\Router\Route\Simple;
use Nomad\Unify\StringMethods;

/**
 * Class Renderer
 * Renders a dispatched instanced object.
 *
 * @package Nomad\Core
 * @author  Mark Hillebert
 */
class Renderer
{
	/**
	 * @var string Theme name
	 */
	protected $_theme;

	/**
	 * @var array Actions to run to provide code for theme. From theme configuration.
	 */
	protected $_blocks = array();

	/**
	 * @var array
	 */
	protected $_css = array();

	/**
	 * @var array
	 */
	protected $_headJs = array();
	protected $_bodyJs = array();

	/**
	 * @var Object The instance we're working on.
	 * @TODO abstract the $_view from controller so other services can have a view. Right now must be a controller
	 */
	protected $_instance;

	/**
	 * @param $instance
	 */
	public function __construct($instance)
	{
		$this->_instance = $instance;
		if ($this->_instance->view) {
			$this->_theme = $this->_instance->view->getTheme();
			if (!$this->_theme) {
				//try to see if it was set in the kernel.yml, if not try to use default
				$namespace         = $this->_instance->getRoute()->getNamespace();
				$namespaceParts    = explode('\\', $namespace);
				$namespaceTopLevel = array_shift($namespaceParts);
				$kernalSetup       = property_exists(Registry::get('packages'), $namespaceTopLevel) ? Registry::get('packages')->$namespaceTopLevel : '';
				if ($kernalSetup && property_exists($kernalSetup, 'theme') && $kernalSetup->theme == $namespaceTopLevel) {
					$this->_theme = $namespaceTopLevel;
				}
				else {
					$this->_theme = Registry::get('application')['config']->default_theme;
				}
			}

		}
	}

	/**
	 * Renders all views, blocks within theme & instance
	 */
	public function render()
	{
		//when using route->browserRedirect('place') this will prevent rendering and removal of flashMessages.
		if (\Nomad\Core\Registry::get('dispatcher')->getContinueToRenderer() !== true) {
			return;
		}
		if (!$this->_theme) {
			return;
		}
		/** @var \Nomad\Core\View $themeView */
		$themeView = new View(
			array(
				'file' => StringMethods::makePath(APPLICATION_ROOT, 'theme', $this->_theme, 'layout', strtolower($this->_theme) . '.phtml')
			));



		$this->_parseThemeConfiguration($this->_theme, $themeView);
		//replace namespacing with slashed
		$this->_theme = preg_replace('~\\\~', '/', $this->_theme);

		$recompileAndCopyAssets = false;
		$productionPreventionFile  = APPLICATION_ROOT . '/theme/DELETE_ME_TO_RECOMPILE_AND_COPY_ASSETS';
		if (ENVIRONMENT == 'development') {
			$recompileAndCopyAssets = true;
		}elseif(ENVIRONMENT == 'production' && !file_exists($productionPreventionFile)) {
			$recompileAndCopyAssets = true;
		}

		if ($recompileAndCopyAssets) {
			//clear public assets folders
			//create the symlinks for assets. NOTE: YOU MUST CREATE THE THEME FOLDER in /public for this to work.
			$publicAssetDirs = glob(StringMethods::makePath(PUBLIC_ROOT, 'assets', '*'), \GLOB_ONLYDIR);
			foreach ($publicAssetDirs as $dir) {
				array_map('unlink', glob("{$dir}/*"));
			}

			//generate any dynamic css files.
			$this->_checkCss();
			$themeAssetDirs = glob(StringMethods::makePath(APPLICATION_ROOT, 'theme', $this->_theme, 'assets', '*'), \GLOB_ONLYDIR);
			foreach ($themeAssetDirs as $dir) {
				$lastPart = substr($dir, strrpos($dir, \DIRECTORY_SEPARATOR) + 1);
				$symDir   = StringMethods::makePath(PUBLIC_ROOT, 'assets', $lastPart);

				// copies all assets to the symDir
				// @TODO: merge all css files into {themeName}.css
				$files = glob($dir . "/*.*");
				foreach ($files as $file) {
					if (!file_exists($symDir)) {
						mkdir($symDir, 0755, true);
					}
					$file_to_go = str_replace($dir, $symDir, $file);
					copy($file, $file_to_go);
				}
			}
			if (ENVIRONMENT == 'production') {
				touch($productionPreventionFile);
			}
		}

		foreach ($this->_blocks as $name => $data) {
			/** @var \Nomad\Core\Dispatcher $dispatcher */
			$dispatcher = Registry::get('dispatcher');
			$block      = $dispatcher->dispatch(
				new Simple(
					array(
						'namespace' => $data['namespace'],
						'action'    => $data['action'],
					)));

			if ($block) {
				if (\get_parent_class($block) == 'Nomad\\Form\\AbstractHtmlForm') {
					$themeView->$name = $block->handle();
				}
				else {
					$block->view->setData(array_merge($this->_instance->view->getData(), $block->view->getData()));
					$themeView->$name = $block->view;
				}
			}
		}

		if (defined('IS_AJAX') && IS_AJAX) {
			echo $this->_instance->view->render();
		}
		else {
			$themeView = $this->_applyLateBindingAco($themeView, null);
			$themeView->content = $this->_instance->view;
			$themeView->setData(array_merge($this->_instance->view->getData(), $themeView->getData()));
			$themeView->setCss(array_merge($this->_instance->view->getCss(), $themeView->getCss()));

			// Merge the scripts from view
			$viewScripts = $this->_instance->view->getScripts();
			$newViewScripts = array(
				'head' => isset($viewScripts['head']) ? $viewScripts['head'] : [],
				'body' => isset($viewScripts['body']) ? $viewScripts['body'] : []
			);
			$themeScripts = $themeView->getScripts();
			$newThemeScripts = array(
				'head' => isset($themeScripts['head']) ? $themeScripts['head'] : [],
				'body' => isset($themeScripts['body']) ? $themeScripts['body'] : []
			);
			$themeView->setScripts(array( 'head' => array_merge($newThemeScripts['head'], $newViewScripts['head']),
										  'body' =>array_merge($newThemeScripts['body'], $newViewScripts['body'])));

			echo $themeView->render();
		}
	}

	/**
	 * @param $view
	 * @return mixed
	 */
	protected function _applyLateBindingAco($view)
	{
		if (Session::getSession()->aco) {
			foreach ($view->getData() as $key => $childView) {
				if (is_object($childView) && get_class($childView) == 'Nomad\\Core\\View') {
					if ($childView->getData()) {
						$this->_applyLateBindingAco($childView);
					}

					if (!Session::getSession()->aco->isPermitted($childView->getCaller())) {
						$view->$key = '';
					}
				}
			}
		}

		return $view;
	}

	/**
	 * Parse a theme configuration for blocks
	 *
	 * @param      $themeName
	 * @param View $themeView
	 * @throws \Nomad\Exception\Argument
	 */
	protected function _parseThemeConfiguration($themeName, \Nomad\Core\View &$themeView)
	{
		$yamlParser = \Nomad\Configuration\Factory::create('yml');
		$allThemes  = $yamlParser->parse(\Nomad\Unify\StringMethods::makePath(APPLICATION_ROOT, 'resource', 'config', 'themes.yml'));

		foreach ($allThemes as $name => $theme) {
			if ($name == $themeName) {
				if (isset($theme->blocks)) {
					foreach ($theme->blocks as $block) {
						foreach ($block as $property => $value) {
							$valueParts               = explode('::', $value);
							$this->_blocks[$property] = ['namespace' => $valueParts[0], 'action' => $valueParts[1]];
						}
					}
				}
				if (isset($theme->css)) {
					foreach ($theme->css as $cssFilename) {
						if (is_string($cssFilename)) {
							$themeView->appendCss($cssFilename);
						}elseif (is_object($cssFilename)) {
							copy(
								\Nomad\Unify\StringMethods::makePath(APPLICATION_ROOT,'theme',$themeName,$cssFilename->source),
								\Nomad\Unify\StringMethods::makePath(PUBLIC_ROOT,$cssFilename->destination)
							);
							if (!property_exists($cssFilename,'append') || (property_exists($cssFilename, 'append') && $cssFilename->append == true)) {
								$themeView->appendCss($cssFilename->destination);
							}
						}
					}
				}

				if (isset($theme->fonts)) {
					foreach ($theme->fonts as $font) {
						if (is_string($font)) {
							// Must use object {source: , destination: }
						}elseif (is_object($font)) {
							copy(
								\Nomad\Unify\StringMethods::makePath(APPLICATION_ROOT,'theme',$themeName,$font->source),
								\Nomad\Unify\StringMethods::makePath(PUBLIC_ROOT,$font->destination)
							);
						}
					}
				}


				if (isset($theme->scripts)) {
					//head scripts
					if (property_exists($theme->scripts, 'head')){
						foreach ($theme->scripts->head as $scriptSrc) {
							if (is_string($scriptSrc)) {
								$themeView->appendHeadSCript($scriptSrc);
							}elseif(is_object($scriptSrc)) {
								copy(
									\Nomad\Unify\StringMethods::makePath(APPLICATION_ROOT,'theme',$themeName,$scriptSrc->source),
									\Nomad\Unify\StringMethods::makePath(PUBLIC_ROOT,$scriptSrc->destination)
								);
								if (!property_exists($scriptSrc,'append') || (property_exists($scriptSrc, 'append') && $scriptSrc->append == true)) {
									$themeView->appendHeadScript($scriptSrc->destination);
								}
							}
						}
					}
					//body scripts
					if (property_exists($theme->scripts, 'body')){
						foreach ($theme->scripts->body as $scriptSrc) {
							if (is_string($scriptSrc)) {
								$themeView->appendBodySCript($scriptSrc);
							}elseif(is_object($scriptSrc)) {
								copy(
									\Nomad\Unify\StringMethods::makePath(APPLICATION_ROOT,'theme',$themeName,$scriptSrc->source),
									\Nomad\Unify\StringMethods::makePath(PUBLIC_ROOT,$scriptSrc->destination)
								);
								if (!property_exists($scriptSrc,'append') || (property_exists($scriptSrc, 'append') && $scriptSrc->append == true)) {
									$themeView->appendBodyScript($scriptSrc->destination);
								}
							}

						}
					}
				}


				break;
			}
		}
	}

	/**
	 * Check the css and recompile if needed
	 */
	protected function _checkCss()
	{

		$pcssFolder = StringMethods::makePath(APPLICATION_ROOT, 'theme', $this->_theme, 'pcss');
		if (!file_exists($pcssFolder)) {
			return;
		}

		foreach (glob(StringMethods::makePath($pcssFolder, "*")) as $filename) {
			if (!is_dir($filename)) {
				$firstLine = $rest = "";
				if ($f = fopen($filename, 'r')) {
					$firstLine = fgets($f); // read until first newline
					$rest      = \stream_get_contents($f);
					\fclose($f);

					$meta = \json_decode($firstLine);

					$newHash = \md5($rest);
					if (!isset($meta->hash) || $meta->hash != $newHash || (isset($_SERVER['ENVIRONMENT']) && $_SERVER['ENVIRONMENT'] == 'development')) {
						$this->_rewriteCss($filename, $newHash, $rest);
					}
				}
			}
		}
	}

	/**
	 * @param $pcssfile
	 * @param $newHash
	 * @param $body '
	 */
	protected function _rewriteCss($pcssfile, $newHash, $body)
	{
		$newMeta = \json_encode(array('hash' => $newHash));
		\file_put_contents($pcssfile, $newMeta . PHP_EOL . $body);

		\ob_start();
		//require_once(StringMethods::makePath(VENDOR_ROOT, 'Nomad', 'src', 'Xcss', 'Color.php'));
		$color = new \Nomad\Xcss\Color();
		$asset = new \Nomad\Xcss\Asset($this->_theme);
		include($pcssfile);
		$css = \ob_get_contents();
		\ob_end_clean();

		$newFilename = preg_replace('~.php$~', '', $pcssfile);
		$newFilepath = preg_replace("~" . $this->_theme . "/pcss~", $this->_theme . '/assets/css', $newFilename);

		if (isset($_SERVER['ENVIRONMENT']) && $_SERVER['ENVIRONMENT'] == 'production') {
			// Remove comments:
			$css = preg_replace('~/\*[^*]*\*+([^/][^*]*\*+)*/~', '', $css);
			// Remove tabs, excessive spaces and newlines
			$css = preg_replace("~(\r\n)|(\r)|(\n)|(\t)~", "", $css);
			// Remove excess spacing
			$css = preg_replace("~[ ]{2,}~", " ", $css);
			// Remove hash from css file
			$css = preg_replace("~\{\".*\":\"[a-f0-9]{32}\"\}~", "", $css);
		}

		\file_put_contents($newFilepath, $css);
	}
}