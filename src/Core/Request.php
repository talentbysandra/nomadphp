<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Core;
/**
 * Class Request
 * Holds all the relevant information of the request
 *
 * @package Nomad\Core\
 * @author  Mark Hillebert
 */
class Request
	extends BaseClass
{
	/**
	 * @var string Raw url
	 */
	protected $_rawUrl;

	//@TODO add more $_SERVER[] stuff here
	/**
	 * @param array $options
	 */
	public function __construct($options = array())
	{
		//set the php server request vars here

		$options['inspector'] = false;

		parent::__construct($options);
	}

	/**
	 * @return bool
	 */
	public function isPost()
	{
		return $_SERVER['REQUEST_METHOD'] === 'POST';
	}
}