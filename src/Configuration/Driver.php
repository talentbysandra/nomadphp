<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Configuration;

use Nomad\Core as Core;
use Nomad\Exception as Exception;

/**
 * Class Driver
 *
 * @package Nomad\Configuration
 * @author  Mark Hillebert
 *
 */
class Driver extends Core\BaseClass
{
	/**
	 * @var array //@TODO ??!@ whats this
	 */
	protected $_parsed = array();

	/**
	 * @return $this
	 */
	public function initialize()
	{
		return $this;
	}
}