<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Configuration\Driver;

use Nomad\Configuration as Configuration;
use Nomad\Unify\ArrayMethods as ArrayMethods;
use Nomad\Exception as Exception;
use Spyc as Spyc; //@todo ensure this is correct class. if so, probable can remove this

/**
 * Class Yaml
 *
 * @package Nomad\Configuration\Driver
 * @author  Mark Hillebert
 */
class Yaml
	extends Configuration\Driver
{
	/**
	 * Parse a yaml file using spyc
	 *
	 * @param $path
	 * @throws \Nomad\Exception\Argument
	 * @throws \Nomad\Exception\Syntax
	 * @return \stdClass
	 */
	public function parse($path)
	{
		if (!$path) {
			throw new Exception\Argument("Cannot parse an empty path.");
		}
		$filename = substr($path, -4, 4) == '.yml' ? $path : $path . '.yml';
		if (!file_exists($filename)) {
			throw new Exception\Syntax("Cannot find '{$filename}' to parse.");
		}
		$array = \Spyc::YAMLLoad($filename);

		return ArrayMethods::toObject($array);
	}
}