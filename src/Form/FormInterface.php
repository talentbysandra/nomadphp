<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form;
/**
 * Interface FormInterface
 *
 * @package Nomad\Form
 * @author  Mark Hillebert
 */
interface FormInterface
{
	/**
	 * Default function to run
	 */
	function initialize();

	/**
	 * @param       $type
	 * @param array $params
	 * @return mixed
	 */
	function createElement($type, $params = array());

	/**
	 * @param $callable
	 * @return mixed
	 */
	function addSuccessHandler($callable);

	/**
	 * @param $callable
	 * @return mixed
	 */
	function addFailHandler($callable);

	/**
	 * @param $type
	 * @return mixed
	 */
	function removeHandlers($type);

	/**
	 * @return mixed Executes the form and performs the handling of submission.
	 */
	function handle();

	/**
	 * @return mixed Renders the form
	 */
	function render();
}