<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form\Element;

use Nomad\Exception\Form;

/**
 * Class Select
 *
 * @package Nomad\Form\Element
 * @author  Mark Hillebert
 */
class Select
	extends AbstractElement
{
	/**
	 * @var array
	 */
	protected $_options;

	/**
	 * @var string
	 */
	protected $_optionTag;

	/**
	 * @var null
	 */
	protected $_originalValue;

	/**
	 * @var string Use this OR optionTag/optionTagWrapper to separate the option;
	 */
	protected $_optionSeparator = '<br/>';

	/**
	 * @param string $name
	 * @param array  $params
	 * @throws Form
	 */
	public function __construct($name, $params = array())
	{
		if (!isset($params['options'])) {
			throw new Form("Radio button must be passed an options array during initialization.");
		}

		$this->_originalValue = isset($params['value']) ? $params['value'] : null;
		parent::__construct($name, $params);
	}

	/**
	 * Renders Element
	 *
	 * @return string
	 * @throws Form
	 */
	public function renderElement()
	{
		$nameHtml    = count($this->_options) > 1 ? "name='{$this->_name}[]'" : "name='{$this->_name}'";
		$elementHtml = "<select type='select' {$nameHtml} {$this->_requiredHtml} {$this->_attributesHtml}>";
		foreach ($this->_options as $key => $option) {
			$valueString = "value='{$key}'";

			if (is_array($this->_value)) {
				$selectedString = !in_array($key, $this->_value) ? "" : "selected='selected'";
			}
			else {
				$selectedString = $this->_value == $key ? "selected='selected'" : "";
			}
			if (is_array($option)) {
				//parameterized options were given
				//'text' must be set
				if (!isset($option['text'])) {
					throw new Form('Select options when passed as parameterized array must pass in `text` key.');
				}
				$htmlAttributes = isset($option['attributes']) ? $this->_createAttributeString($option['attributes']) : null;
				$optionHtml     = "<option {$htmlAttributes} {$valueString}>{$option['text']}</option>";
			}
			else {
				$optionHtml = "<option {$selectedString} {$valueString}>{$option}</option>";
			}

			$elementHtml .= $optionHtml;
		}

		return $elementHtml . "</select>";
	}

	/**
	 * Internal check for isValid.
	 * Make sure the select values is one of the 'set' values and hasn't been altered.
	 *
	 * @param array $formValues
	 * @return bool
	 */
	public function isValid($formValues = array())
	{
		if ($this->_required && is_null($this->getValue())) {
			return false;
		}

		$selectedValue = $this->getValue();
		if ((!is_string($selectedValue) || !\array_key_exists($selectedValue, $this->_options))) {
			$this->_errorMessages[] = "Invalid selection.";
			$this->_value           = $this->_originalValue;

			return false;
		}

		return true;
	}

	/**
	 * Gets value of element. From POST first, then default value (if any) or null if none;
	 *
	 * @return null
	 */
	public function getValue()
	{
		return $this->_value[0];
	}
}