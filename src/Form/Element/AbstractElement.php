<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form\Element;

use Nomad\Exception as Exception;

/**
 * Class AbstractElement
 *
 * @todo    clean this up to use attributes and not individual properties for attributes... //Work in progress
 * @package Nomad\Form\Element
 * @author  Mark Hillebert
 */
abstract class AbstractElement
{
	/**
	 * @var string
	 */
	protected $_elementType = 'null';

	/**
	 * @var string
	 */
	protected $_name;

	/**
	 * @var string Label text
	 */
	protected $_label;

	/**
	 * @var array of attributes for label tag
	 */
	protected $_labelAttributes;

	/**
	 * @var string Position of label (before/after) values are 'prepend' or 'append'
	 */
	protected $_labelPlacement = 'prepend';

	/**
	 * @var string
	 */
	protected $_description;
	/**
	 * @var string
	 */
	protected $_value;
	/**
	 * @var string
	 */
	protected $_valueHtml;

	/**
	 * @var array
	 */
	protected $_validators = array();
	/**
	 * @var array
	 */
	protected $_filters = array();
	/**
	 * @var array
	 */
	protected $_attributes;
	/**
	 * @var string
	 */
	protected $_attributesHtml;

	/**
	 * @var bool
	 */
	protected $_required;
	/**
	 * @var string
	 */
	protected $_requiredHtml;

	//entire wrapper
	protected $_elementWrapper = 'li';
	protected $_wrapperTagClass; //@deprecated; use wrapperAttributes array
	protected $_elementWrapperErrorClass = 'error';
	protected $_wrapperAttributes = array();

	//label
	protected $_labelTag = 'label';
	protected $_labelClass;//@deprecated -- use label attributes instead
	protected $_labelErrorClass = 'error';

	//description text
	protected $_descriptionTag = 'small';
	protected $_descriptionErrorClass;

	//element itself
	protected $_elementErrorClass = 'error';

	//error messages wrapper
	protected $_errorsWrapperTag;
	protected $_errorsWrapperTagClass;

	//error message itself
	protected $_errorTag = 'small';
	protected $_errorTagClass = 'error';

	public $rendered = false;
	protected $_isWrapped;
	/**
	 * Error Messages
	 *
	 * @var array
	 */
	protected $_errorMessages = array();

	/**
	 * Has passed validation
	 *
	 * @var bool
	 */
	protected $_isValid = true;

	/**
	 * Determines whether this element is included when passing back data
	 *
	 * @var boolean
	 */
	protected $_ignore;

	/**
	 * Determine whether to include element errors on the render function
	 *
	 * @var bool
	 */
	public $useCustomErrorRendering = false;

	/**
	 * Determines whether to ignore this element in the POST
	 *
	 * @var bool
	 */
	public $ignore = false;

	protected $_beenSubmitted = false;

	/**
	 * @param string $name
	 * @param array  $params OPTIONAL
	 */
	public function __construct($name, $params = array())
	{
		$this->_name = $name;
		$this->_setParams($params);

		//$this->useCustomErrorRendering = $params['useCustomErrorRendering'];
//        if (isset($params['message'])){
//            $this->_message = $params['message'];
//        }

		if (isset($params['ignore'])) {
			$this->ignore = $params['ignore'];
		}
	}

	/**
	 * Sets parameters
	 *
	 * @param array $params
	 * @throws \Nomad\Exception\Form
	 */
	protected function _setParams($params = array())
	{
		foreach ($params as $paramName => $value) {
			$protected = "_" . $paramName;
			if (!\property_exists($this, $protected)) {
				throw new Exception\Form("'{$paramName}' is not a property of {$this->_elementType} element.");
			}

			$this->$protected = $value;
		}

		if (isset($this->_attributes)) {
			$this->_attributesHtml = $this->_createAttributeString($this->_attributes);
		}

		$this->_valueHtml    = !isset($this->_value) ? "" : "value='{$this->_value}'";
		$this->_requiredHtml = !isset($this->_required) ? "" : "required='required'";
	}

	/**
	 * Answers: is this element wrapped in a wrapper?
	 *
	 * @return bool
	 */
	public function isWrapped()
	{
		return (bool)$this->_isWrapped;
	}

	/**
	 * Sets isWrapped to true
	 */
	public function setWrapped()
	{
		$this->_isWrapped = true;
	}

	/**
	 * Renders the element. Handled in each specific type file.
	 *
	 * @return mixed
	 */
	abstract public function renderElement();

	/**
	 * Render the element.
	 * Unless $useCustomErrorRendering is true, this will attach the error messages to the rendering
	 *
	 * @param array $formValues
	 * @return mixed
	 */
	public function render($formValues = array())
	{
		//run filters
		foreach ($this->_filters as $filter) {
			// $this->value = $filter($this->value);
		}
		//run validators
		//handled via form abstract class on POST

		//build element's full html
		// wrapper?
		$outerWrapperHtml    = null;
		$usingElementWrapper = isset($this->_elementWrapper);
		if ($usingElementWrapper) {
			$wrapperClassHtml = "";
			if (isset($this->_wrapperTagClass)) {
				$this->_wrapperAttributes['class'] = $this->_wrapperTagClass; //@deprecated way of setting class in wrapper tags; use wrapperAttributes associated array!!
			}

			foreach ($this->_wrapperAttributes as $attr => $value) //@TODO REWORK THIS ASAP.. this is rendering all as attributes but the name is misleading (wrapperTagCLASS should be wrapperAttributes)
			{
				$wrapperClassHtml .= "{$attr}='{$value}' ";
			}
			$wrapperClassHtml = trim($wrapperClassHtml);

//@TODO MUST CHANGES THIS BECAUSE THE ERROR CLASS WILL MESS UP THE ABOVE (USE WRAPPER ATTRIBUTES?)
			$classString = "";
			$classArray  = array();

			if (isset($this->_attributes['wrapperClass'])) {
				$classArray[] = $this->_attributes['wrapperClass'];
			}

			if (!$this->_isValid) {
				$classArray[] = $this->_elementErrorClass;
			}

			if (!empty($classArray)) {
				$classString = "class='" . implode(' ', $classArray) . "'";
			}
			$outerWrapperHtml = "<{$this->_elementWrapper} {$wrapperClassHtml} {$classString}>";
		}

		//label?
		$labelHtml = $this->renderLabelHtml();

		//element itself
		$elementHtml = $this->renderElement();

		$descriptionHtml = $this->renderDescriptionHtml();

		//errors?
		$errorsHtml = $this->renderErrorsHtml();

		//build ful html string
		if ($this->_labelPlacement == 'prepend') {
			$return = $outerWrapperHtml . $labelHtml . $elementHtml . $errorsHtml . $descriptionHtml . ($usingElementWrapper ? "</{$this->_elementWrapper}>" : "");
		}else{
			$return = $outerWrapperHtml . $elementHtml . $labelHtml . $errorsHtml . $descriptionHtml . ($usingElementWrapper ? "</{$this->_elementWrapper}>" : "");
		}
		return $return;
	}

	/**
	 * Gets value of element. From POST first, then default value (if any) or null if none;
	 *
	 * @return null
	 */
	public function getValue()
	{
		return $this->_value;
	}

	/**
	 * Sets the value of the element
	 *
	 * @param $value
	 */
	public function setValue($value)
	{
		if (property_exists($this, '_originalValue')) {
			$this->_originalValue = $value;
		}
		$this->_value     = $value;
		$this->_valueHtml = !isset($this->_value) ? "" : (is_string($this->_value) ? 'value="' . $this->_value . '"' : "");
	}

	/**
	 * Sets the description of the element
	 *
	 * @param $value
	 */
	public function setDescription($value)
	{
		$this->_description = $value;
	}

	/**
	 * Returns an html string of the attributes passed in the params array
	 *
	 * @return string
	 */
	protected function _renderAttributes()
	{
		return $this->_renderGenericAttributes($this->_attributes);
	}

	/**
	 * Returns an html string of the attributes passed in the params array
	 *
	 * @return string
	 */
	protected function _renderWrapperAttributes()
	{
		return $this->_renderGenericAttributes($this->_wrapperAttributes);
	}

	/**
	 * Returns an html string of the attributes passed in the params array
	 *
	 * @param $array
	 * @return string
	 */
	protected function _renderGenericAttributes($array)
	{
		$attributeString = "";
		if (isset($array)) {
			foreach ($array as $attribute => $value) {
				if ($value) {
					$attributeString .= " {$attribute}='$value'";
				}
			}
		}

		return $attributeString;
	}

	/**
	 * Element render helper. Renders any errors.
	 *
	 * @return string
	 */
	public function renderErrorsHtml()
	{
		$errorsHtml = "";
		if ($this->_isValid == false) {
			$errorsWrapper   = null;
			$doWrapAllErrors = isset($this->_errorsWrapperTag);
			if ($doWrapAllErrors) {
				$errorsWrapperClassHtml = !isset($this->_errorsWrapperTagClass) ? "" : "class='{$this->_errorsWrapperTagClass}'";
				$errorsHtml             = "<{$this->_errorsWrapperTag} {$errorsWrapperClassHtml}>";
			}
			foreach ($this->_errorMessages as $message) {
				$errorClassHtml = !isset($this->_errorTagClass) ? "" : "class='{$this->_errorTagClass}'";
				$errorsHtml .= "<{$this->_errorTag} {$errorClassHtml}>$message</{$this->_errorTag}>";
			}
			if ($doWrapAllErrors) {
				$errorsHtml .= "</{$this->_errorsWrapperTag}>";
			}
		}

		return $errorsHtml;
	}

	/**
	 * Render the label portion of element.
	 *
	 * @since 2015.08.22
	 *
	 * @return string
	 */
	public function renderLabelHtml()
	{
		$labelHtml = null;
		if (isset($this->_label)) {
			$attributes = '';
			if (isset($this->_labelAttributes)) {
				if (!$this->_isValid) {
					if (isset($this->_labelAttributes['class'])) {
						$this->_labelAttributes['class'] .= " {$this->_labelErrorClass}";
					}
				}
				$attributes = ' '. $this->_createAttributeString($this->_labelAttributes);
			}
			$forString = " for='{$this->_attributes['id']}'";
			if ($this->_elementType == 'checkbox' || $this->_elementType == 'select') {
				$forString = '';
			}
			$labelHtml = "<{$this->_labelTag}{$forString}{$attributes}>{$this->_label}</{$this->_labelTag}>";
		}
		return $labelHtml;
	}

	/**
	 * @return mixed
	 */
	public function getLabel()
	{
		return $this->_label;
	}

	/**
	 * Render the description portion of element.
	 *
	 * @return string
	 */
	public function renderDescriptionHtml()
	{
		$labelHtml = null;
		if (isset($this->_description)) {
			$errorClassString = "";
			if (!$this->_isValid) {
				$errorClassString = "class='{$this->_descriptionErrorClass}'";
			}
			$labelHtml = "<{$this->_descriptionTag} {$errorClassString}>{$this->_description}</{$this->_descriptionTag}>";
		}

		return $labelHtml;
	}

	/**
	 * Element's internal validation
	 *
	 * @param array $formValues
	 * @return bool
	 */
	abstract function isValid($formValues = array());

	/**
	 * Get element name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->_name;
	}

	/**
	 * Sets the name of an element
	 *
	 * @param $name string
	 * @return $this
	 */
	public function setName($name)
	{
		$this->_name = $name;

		return $this;
	}

	/**
	 * Filters this element's current value
	 *
	 * @return bool
	 * @throws Exception\Form
	 */
	public function filter()
	{
		$value = $this->_value;
		if (is_array($this->_filters)) {
			foreach ($this->_filters as $filter) {
				//assume the filter is a standard Nomad_Filter class string and not customized
				$params = null;
				if (is_array($filter)) { //the filter is customized

					if (!isset($filter[0]) && !is_string($filter[0])) {
						throw new Exception\Form('Error: ' . $this->_name . '  Filter first param must be a string and the next is array of params.');
					}
					//change the type to the type declared in the filter array
					$type   = $filter[0];
					$params = isset($filter[1]) ? $filter[1] : null;
				}
				else {
					$type = $filter;
				}

				/**
				 * Check for a user created form element class first.
				 * This can (and is allowed) to overwrite nomad classes.
				 * If not found, then try a for a nomad element class, throw exception if no Nomad class exists either
				 */
				$nomadType = "Nomad\\Filter\\" . ucfirst($type);
				if (class_exists($type)) {
					//$this->_elements[$name] = new $type ($name, $params);
					$filterer = new $type($filter, $params);
				}
				elseif (class_exists($nomadType)) {
					//$this->_elements[$name] = new $nomadType ($name, $params);
					$filterer = new $nomadType($filter, $params);
				}
				else {
					throw new Exception\Form('Unknown validator type: ' . $type);
				}

				$value = $filterer->filter($value);
			}
		}
		else {
			throw new Exception\Form('Error: ' . $this->_name . ' - Validators must be an associative array with at least a "type" key.');
		}
		$this->_value = $value;
	}

	/**
	 * Executes validation on element
	 *
	 * @param $validator
	 * @param $formValues
	 * @return bool
	 * @throws \Nomad\Exception\Form
	 */
	protected function _validate($validator, $formValues)
	{
		if (!$validator->isValid($this->_value, $formValues)) {
			if (!isset($validatorParams['errorMessage'])) {
				$this->_errorMessages[] = $validator->getMessage();
			}
			else {
				$this->_errorMessages[] = $validatorParams['errorMessage'];
			}

			return false;
		}

		return true;
	}

	/**
	 * Gets the validator
	 *
	 * @param $validatorParams
	 * @return mixed
	 * @throws \Nomad\Exception\Form
	 */
	protected function _getValidator($validatorParams)
	{
		if (is_string($validatorParams)) {
			$validatorParams = array('type' => $validatorParams);
		}
		$nomadType = "Nomad\\Validator\\" . ucfirst($validatorParams['type']);
		if (class_exists($validatorParams['type'])) {
			$params    = isset($validatorParams['value']) ? $validatorParams['value'] : null; // why am i doing this and not just passing in the validatorParams?
			$params    = $validatorParams;
			$validator = new $validatorParams['type']($params);
		}
		elseif (class_exists($nomadType)) {
			$validator = new $nomadType($validatorParams);
		}
		else {
			throw new Exception\Form('Unknown validator type: ' . $validatorParams);
		}

		return $validator;
	}

	/**
	 * @param array $formValues The values from the form.
	 * @return bool
	 */
	public function checkValidity($formValues = array())
	{
		//run validators
		$this->_isValid = true;
		if (!empty($formValues)) {
			foreach ($this->_validators as $validator) {
				$validatorObj = $this->_getValidator($validator);
				$this->_isValid &= $this->_validate($validatorObj, $formValues);
				if (!$this->_isValid && $validatorObj->willBreakChain()) {
					return false;
				}
			}
			//run element's isValid()
			$this->_isValid &= $this->isValid($formValues);
		}

		return $this->getValidity();
	}

	/**
	 * Returns the current state of validation
	 *
	 * @return bool
	 */
	public function getValidity()
	{
		return $this->_isValid;
	}

	/**
	 * Helper function to create the attributes of a tag
	 *
	 * @param $attributes
	 * @return string
	 */
	protected function _createAttributeString($attributes)
	{
		$html = '';
		if (is_array($attributes)) {
			foreach ($attributes as $attrName => $attrValue) {
				$html .= " {$attrName}='{$attrValue}'";
			}

			return ltrim($html);
		}
		else {
			return $attributes;
		}
	}

	/**
	 * Sets the class on the wrapper tag
	 *
	 * @param $className string
	 */
	public function setWrapperClass($className)
	{
		$this->_attributes['wrapperClass'] = $className;
	}

	/**
	 * Removes the label
	 */
	public function removeLabel()
	{
		$this->_label = null;
	}

	/**
	 * @return string Element type
	 */
	public function getType()
	{
		return $this->_elementType;
	}

	/**
	 * Gets an attribute value or null
	 *
	 * @param $name
	 * @return mixed|null
	 */
	public function getAttribute($name)
	{
		return isset($this->_attributes[$name]) ? $this->_attributes[$name] : null;
	}

	/**
	 * Does this element have any filters?
	 *
	 * @return bool
	 */
	public function hasFilter()
	{
		return !empty($this->_filters);
	}

	/**
	 * Setter for  _hasBeenSubmitted
	 * Tells the element if it has been submitted by user
	 *
	 * @param $boolean
	 */
	public function hasBeenSubmitted($boolean)
	{
		$this->_beenSubmitted = $boolean;
	}

	/**
	 * Getter for _beenSubmitted
	 *
	 * @return bool
	 */
	public function wasSubmitted()
	{
		return $this->_beenSubmitted;
	}

	/**
	 * has 'type'=> validatorName, other_params => blah
	 *
	 * @param array $validatorArray
	 */
	public function addValidator(array $validatorArray)
	{
		$this->_validators[] = $validatorArray;
	}

	/**
	 * has 'type'=> validatorName, other_params => blah
	 *
	 * @param array $filterArray
	 */
	public function addFilter(array $filterArray)
	{
		$this->_filters[] = $filterArray;
	}

	public function setWrapperTag($tag)
	{
		$this->_elementWrapper = $tag;
	}

	public function setWrapperAttributes(array $attributes)
	{
		$this->_wrapperAttributes = $attributes;
	}
}