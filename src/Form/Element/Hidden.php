<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form\Element;

/**
 * Class Hidden
 *
 * @package Nomad\Form\Element
 * @author  Mark Hillebert
 */
class Hidden
	extends AbstractElement
{
	/**
	 * Renders Element
	 *
	 * @return string
	 */
	public function renderElement()
	{
		return "<input type='hidden' name='{$this->_name}' {$this->_attributesHtml} {$this->_valueHtml} {$this->_requiredHtml}/>";
	}

	/**
	 * @param array $formValuesArray
	 * @return bool
	 */
	public function isValid($formValuesArray = array())
	{
		return true;
	}
}