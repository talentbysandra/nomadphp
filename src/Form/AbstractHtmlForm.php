<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form;

use Nomad\Core\Session;
use Nomad\Core\View;
use Nomad\Exception as Exception;

/**
 * Class AbstractHtmlForm
 *
 * @package Nomad\Form
 * @author  Mark Hillebert
 */
class AbstractHtmlForm
	implements FormInterface
{

	/**
	 * Element name for the csrf
	 */
	const CSRF_TOKEN_NAME = 'csrfToken';

	/**
	 * @var bool
	 */
	public $initialized = false;

	/**
	 * @var string name of form
	 */
	public $name;

	/**
	 * @var string Form's action
	 */
	public $action;

	/**
	 * @var string Form's method
	 */
	public $method = 'post';

	/**
	 * @var string Title for form.
	 */
	public $title;
	public $titleTag = 'h2';
	public $titleClass;

	/**
	 * @var array attributes on form tag
	 */
	public $attributes;

	/**
	 * This will wrap all elements in this tag
	 *
	 * @var string Element Wrapper. Use if elements are in an html list
	 */
	protected $_elementsWrapperTag = 'ul';
	protected $_elementsWrapperAttributes;

	/**
	 * These are for each element NOTICE: element != elements
	 * @var string
	 */
	protected $_elementWrapperTag = 'li';
	protected $_elementWrapperAttributes;

	/**
	 * @var string Error Message for entire form
	 */
	public $formErrorMessage;
	protected $_formErrorMessageTag = 'small';
	protected $_formErrorMessageClass = 'error';
	protected $_formErrorClass = 'error';

	/**
	 * @var string Success Message for entire form
	 */
	public $formSuccessMessage;
	protected $_formSuccessMessageTag = 'small';
	protected $_formSuccessMessageClass = 'success';
	protected $_formSuccessClass = 'success';

	/**
	 * @var array HTML Elements
	 */
	protected $_elements = array();

	/**
	 * @var bool
	 */
	protected $_displayFormTag = true;

	/**
	 * @var
	 */
	protected $_formIdentifier;

	/**
	 * functions/methods to execute when passes validation
	 *
	 * @var array
	 */
	protected $_successHandlers = array();

	/**
	 * functions/methods to execute when failed validation
	 *
	 * @var array
	 */
	protected $_failureHandlers = array();

	/**
	 * @var bool
	 */
	protected $_hasPassedValidation = true;

	/**
	 * @var string The name of the submit button. Used when checking POST to determine which form was POSTed.
	 * There may be a better way of handling pages with multiple forms.
	 */
	protected $_submitButtonName;

	/**
	 * @var string The annotated style string to define a custom view to use to render the form.
	 */
	protected $_view;

	/**
	 * Access Control Object
	 *
	 * @var \Nomad\Container\AccessControl\Aco
	 */
	protected $_useCsrf = true;

	/**
	 * @var mixed Defaults property to set in params to populate form with (handled by each form)
	 */
	protected $_defaults;

	/**
	 * Ignore these elements for csrf token creation
	 *
	 * @var array
	 */
	protected $_ignoredCsrfElements = array(
		'Nomad\\Form\\Element\\Html',
		'Nomad\\Form\\Element\\Wrapper',
		'Nomad\\Form\\Element\\Link'
	);

	/**
	 * Constructor. Will always call the initialize() method.
	 *
	 * @param array $params All other form parameters in associative array.
	 */
	public function __construct($params = array())
	{
		if (is_object($params)) {
			// If the params is an object, set it as the _defaults
			$this->_defaults = $params;
		}
		else {
			if (is_array($params)) {

				foreach ($params as $property => $value) {
					$protectedName        = '_' . $property;
					$this->$protectedName = $value;
				}
			}
		}
		if (!$this->name) {
			//generate a random name for the form
			$this->name = 'form' . rand(0, 9);
		}

		//check for default Handler
		$className   = get_class($this);
		$handlerName = substr_replace($className, '\\Handler', strrpos($className, '\\'), 0);
		if (\class_exists($handlerName)) {
			//assume the class is a child of Nomad\Form\AbstractHandler
			$this->addFormHandler($handlerName);
		}

		//@TODO this is causing the method to be run twice when set as a block in a theme
		if (!$this->initialized) {
			$this->initialize();
		}

		return $this;
	}

	/**
	 * Gets the _defaults variable
	 *
	 * @return mixed|null
	 */
	public function getDefaults()
	{
		return $this->_defaults;
	}

	/**
	 * Sets the _defaults variable
	 *
	 * @param $value
	 * @return mixed|null
	 */
	public function setDefaults($value)
	{
		$this->_defaults = $value;
	}

	/**
	 * Disables the auto-csrf
	 */
	public function disableCsrf()
	{
		$this->_useCsrf = false;
	}

	/**
	 * Disables form tag
	 */
	public function disableFormTag()
	{
		$this->_displayFormTag = false;
	}

	/**
	 * Default function to run
	 */
	public function initialize()
	{
		throw new Exception\Form("Please create and use an 'initialize()' method in your form.");
	}

	/**
	 * Adds csrf
	 *
	 * @throws Exception\Form
	 */
	public function addCsrf()
	{
		$this->createElement(
			'hidden', array(
			'name'          => self::CSRF_TOKEN_NAME,
			'value'         => 0,
			'validators'    => array('csrf'),
			'errorTagClass' => 'error formError',
			'ignore'        => true
		));

		$formIdentifier = crc32(implode('', $this->_getSortedKeys()));
		$sessionCsrf    = Session::getSession()->$formIdentifier;

		if ($sessionCsrf) {
			//set its value to what we have
			$this->getElement(self::CSRF_TOKEN_NAME)->setValue($sessionCsrf['token']);
		}
		else {
			//set new token on both element and session
			$time  = time();
			$token = md5($this->name . $time);
			$this->getElement(self::CSRF_TOKEN_NAME)->setValue($token);
			$sessionCsrf = array(
				'token'     => $token,
				'timestamp' => $time
			);

			Session::getSession()->$formIdentifier = $sessionCsrf;
		}
	}

	/**
	 * Regenerates the csrf token
	 */
	protected function _regenerateCsrf()
	{
		$time  = time();
		$token = md5($this->name . $time);

		$formIdentifier = crc32(implode('', $this->_getSortedKeys()));
		$this->_elements[self::CSRF_TOKEN_NAME]->setValue($token);
		Session::getSession()->$formIdentifier = array(
			'token'     => $token,
			'timestamp' => $time
		);
	}

	/**
	 * Renders the form. Returns the resulting html
	 *
	 * @return string
	 */
	public function render()
	{

		if (isset($this->_view)) {
			$this->_view->{$this->name} = $this; //put the whole form into the view for access
			return $this->_view->render();
		}
		else {
			$elementsHtmlString = $closingElementsHtmlString = '';
			//create title
			$titleHtmlString = '';
			if ($this->title) {
				$titleClassHtml  = isset($this->titleClass) ? "class='{$this->titleClass}'" : '';
				$titleHtmlString = "<{$this->titleTag} {$titleClassHtml}>{$this->title}</{$this->titleTag}>";
			}


			if (isset($this->_elementsWrapperTag)) {
				$elementsWrapperAttributes = '';
				if (isset($this->_elementsWrapperAttributes)) {
					$elementsWrapperAttributes = ' ' . $this->_createAttributeString($this->_elementsWrapperAttributes);
				}

				$elementsHtmlString        = "<{$this->_elementsWrapperTag}{$elementsWrapperAttributes}>";
				$closingElementsHtmlString = "</{$this->_elementsWrapperTag}>";
			}

			//process any wrappers
			$this->_moveToWrappers();

			foreach ($this->_elements as $element) {
				if ($element->rendered) {
					continue;
				}

				if (isset($this->_elementWrapperTag)) {
					$element->setWrapperTag($this->_elementsWrapperTag);
				}
				if (isset($this->_elementWrapperAttributes)) {
					$element->setWrapperAttributes($this->_elementWrapperAttributes);
				}

				$elementsHtmlString .= $element->render($this->_mandatoryUserInputFilter($_POST));
				$this->_hasPassedValidation &= $element->getValidity();
			}
			$formErrorMessageHtml = $formErrorClassHtml = '';
			if (!$this->_hasPassedValidation) {
				$formErrorClassHtml = "class='{$this->_formErrorClass}'";
			}

			$attributesHtml = '';
			if (isset($this->attributes)) {
				if (is_array($this->attributes)) {
					foreach ($this->attributes as $attr => $value) {
						$attributesHtml .= "{$attr}='{$value}' ";
					}
					$attributesHtml = trim($attributesHtml);
				}
				else {
					$attributesHtml = $this->attributes;
				}
			}

			if ($this->_hasPassedValidation && isset($this->formErrorMessage)) {
				$errorClassString     = !isset($this->_formErrorMessageClass) ? "" : "class='{$this->_formErrorMessageClass}'";
				$formErrorMessageHtml = "<{$this->_formErrorMessageTag} {$errorClassString}>{$this->formErrorMessage}</{$this->_formErrorMessageTag}>";
				$formErrorClassHtml   = "class='{$this->_formErrorClass}'";
			}
			$formSuccessMessageHtml = '';
			$formSuccessClassHtml   = '';
			if ($this->_hasPassedValidation && isset($this->formSuccessMessage)) {
				$successClassString     = !isset($this->_formSuccessMessageClass) ? "" : "class='{$this->_formSuccessMessageClass}'";
				$formSuccessMessageHtml = "<{$this->_formErrorMessageTag} {$successClassString}>{$this->formSuccessMessage}</{$this->_formSuccessMessageTag}>";
				$formSuccessClassHtml   = "class='{$this->_formSuccessClass}'";
			}

			if ($this->_displayFormTag) {
				$htmlString = "{$titleHtmlString}<form name='{$this->name}' action='{$this->action}' method='{$this->method}'  {$attributesHtml} {$formErrorClassHtml} {$formSuccessClassHtml}>";
				$htmlString .= $elementsHtmlString . $closingElementsHtmlString . $formErrorMessageHtml . $formSuccessMessageHtml . "</form>";
			}
			else {
				$htmlString = $elementsHtmlString . $closingElementsHtmlString;
			}
			// Session::getSession()->forms[] = sha1(array_keys($this->_elements));
		}

		return $htmlString;
	}

	/**
	 * move to wrappers
	 */
	protected function _moveToWrappers()
	{
		foreach ($this->_elements as $element) {
			if ($element instanceof \Nomad\Form\Element\Wrapper) {
				foreach ($element->getWrapped() as $wrapped) {
					$wrappedElement = $this->getElement($wrapped);
					$wrappedElement->setWrapped(true);
					$element->addElement($wrappedElement);
				}
			}
		}
	}

	/**
	 * Gets any wrapper elements
	 *
	 * @param bool $excludeWrapperType
	 * @return array
	 */
	public function getRootWrapperElements($excludeWrapperType = true)
	{
		$this->_moveToWrappers();
		$rootWrapperElements = array();

		foreach ($this->_elements as $element) {
			if ($excludeWrapperType) {
				if ($element instanceof \Nomad\Form\Element\Wrapper && $element->isWrapped() !== true) {
					$rootWrapperElements[] = $element;
				}
			}
		}

		return $rootWrapperElements;
	}

	/**
	 * Executes the form and runs any handlers if needed.
	 *
	 * @return array|mixed|null|string
	 */
	public function handle()
	{
		if ($this->_useCsrf) {
			$this->addCsrf();
		}

		$return = null;
		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST[$this->_submitButtonName])) {
			$values = $this->_mandatoryUserInputFilter($_POST);

			if (isset($_FILES)) {
				//must merge in $_FILES into values for csrf to work properly
				$values = $values + $_FILES; //NOTE! if the name of the element is_numeric == true, then it will fail.
			}

			/** @var $element \Nomad\Form\Element\AbstractElement */
			foreach ($this->_elements as $element) {
				$elementName = $element->getName();
				if (isset($values[$elementName])) {
					$element->setValue($values[$elementName]);
				}
				else {
					if (!$this->_isIgnoredClass($element)) {
						//some elements like radio buttons do not pass in values -- so we'll set it to an empty array
						$element->setValue(array());
						$values[$element->getName()] = array();
					}
				}
				$element->hasBeenSubmitted(true);

				if ($element->hasFilter()) {
					$element->filter();
				}

				$this->_hasPassedValidation &= (bool)$element->checkValidity($values);
				if ($element->getName() == self::CSRF_TOKEN_NAME) {
					$this->_regenerateCsrf();
				}
			}

			if ((bool)$this->_hasPassedValidation == true) {
				if (method_exists($this, 'isValid')) {
					//check if the user added an isValid method and run it.
					if ($this->isValid()) {
						$return = $this->_runPassHandlers();
					}
					else {
						$return = $this->_runFailHandlers();
					}
				}
				else {
					$return = $this->_runPassHandlers();
				}
			}
			else {
				//run fail handlers
				$return = $this->_runFailHandlers();
			}
		}
		else {
			$return = $this->render();
		}

		if ($return === "0" || $return === "1") {
			//Handlers may return content, or true/false; when returned as true/false it comes back as "1" or "0"
			//So, convert these to proper boolean.
			$return = (bool)$return;
		}

		return $return;
	}

	/**
	 * Performs a basic filtering on all input values from form.
	 *
	 * @param array $postValues
	 * @return array
	 */
	protected function _mandatoryUserInputFilter(array $postValues)
	{
		//do global filtering here -- this does not replace the validation that needs to take place on each element -- just the basics
		$newArray = array();
		foreach ($postValues as $key => $value) {
			if (is_array($value)) {
				$valueArray = array();
				foreach ($value as $subKey => $subValue) {
					$subKey              = trim($subKey);
					$subKey              = filter_var(
						$subKey, \FILTER_UNSAFE_RAW, array(
						'flags' => array(
							\FILTER_FLAG_STRIP_LOW,
							\FILTER_FLAG_STRIP_HIGH
						)
					));
					$subValue            = trim($subValue);
					$subValue            = filter_var(
						$subValue, \FILTER_UNSAFE_RAW, array(
						'flags' => array(
							\FILTER_FLAG_STRIP_LOW,
							\FILTER_FLAG_STRIP_HIGH
						)
					));
					$valueArray[$subKey] = $subValue;
				}
				$value = $valueArray;
			}
			else {
				$key   = trim($key);
				$key   = filter_var(
					$key, \FILTER_UNSAFE_RAW, array(
					'flags' => array(
						\FILTER_FLAG_STRIP_LOW,
						\FILTER_FLAG_STRIP_HIGH
					)
				));
				$value = trim($value);
				$value = filter_var(
					$value, \FILTER_UNSAFE_RAW, array(
					'flags' => array(
						\FILTER_FLAG_STRIP_LOW,
						\FILTER_FLAG_STRIP_HIGH
					)
				));
			}

			$newArray[$key] = $value;
		}

		return $newArray;
	}

	/**
	 * adds a function/method to successHandlers array
	 *
	 * @param $callable
	 */
	public function addSuccessHandler($callable)
	{
		//If a Nomad_Form_Handler was added, it contains both a success and failure handler... make sure we add to both.
		if (is_subclass_of($callable, 'Nomad\Form\HandlerAbstract')) {
			$this->_successHandlers[] = array($callable, 'onSuccess');
		}
		$this->_successHandlers[] = $callable;
	}

	public function addClass($className)
	{
		if (isset($this->attributes['class'])) {
			$this->attributes['class'] = trim($this->attributes['class'] . ' ' . $className);
		}
		else {
			$this->attributes['class'] = trim($className);
		}
	}

	/**
	 * Adds a function/method to failureHandlers array
	 *
	 * @param $callable
	 */
	public function addFailHandler($callable)
	{
		//If a Nomad_Form_Handler was added, it contains both a success and failure handler... make sure we add to both.
		if (is_subclass_of($callable, 'Nomad\Form\HandlerAbstract')) {
			$this->_failureHandlers[] = array($callable, 'onFailure');
		}
		else {
			$this->_failureHandlers[] = $callable;
		}
	}

	/**
	 * @param string $type Removes handlers based on type ('success'|'fail') defaults to removing ALL handlers.
	 */
	public function removeHandlers($type = 'all')
	{
		switch ($type) {
			case 'success':
				$this->_successHandlers = array();
				break;
			case 'fail':
				$this->_failureHandlers = array();
				break;
			default;
				$this->_failureHandlers = array();
				$this->_successHandlers = array();
		}
	}

	/**
	 * Adds a Nomad_Form_Handler to success and failure array.
	 *
	 * @param $handlerClassName
	 */
	public function addFormHandler($handlerClassName)
	{
		$this->_failureHandlers[] = array($handlerClassName, 'onFailure');
		$this->_successHandlers[] = array($handlerClassName, 'onSuccess');
	}

	/**
	 * Execute the methods/functions contains within the successHandler array
	 *
	 * @throws \Nomad\Exception\Form
	 * @return array|null|string
	 */
	protected function _runPassHandlers()
	{
		$return = null;
		if (!empty($this->_successHandlers)) {
			foreach ($this->_successHandlers as $successFunction) {
				if (is_array($successFunction)) {
					//assume 2 params [0] = class, [1] = method to run (always passing in $this form)
					$successHandler = new $successFunction[0];
					$return .= $successHandler->$successFunction[1]($this);
				}
				elseif (is_string($successFunction) && class_exists($successFunction)) {
					$handler = new $successFunction();
					$return .= $handler($this);
				}
				elseif (is_object($successFunction) && method_exists($successFunction, 'onSuccess')) {
					$return .= $successFunction->onSuccess($this);
				}
			}
		}

		return $return;
	}

	/**
	 * Execute the methods/functions contains within the failureHandler array
	 *
	 * @return array|null|string
	 * @throws Exception\Form
	 */
	protected function _runFailHandlers()
	{
		$return = null;
		if (!empty($this->_failureHandlers)) {
			foreach ($this->_failureHandlers as $failureClass=>$failureFunction) {
				if (is_array($failureFunction)) {//assume 2 params [0] = class, [1] = method to run (always passing in $this form)
					$failHandler = new $failureFunction[0];
					$return .= $failHandler->$failureFunction[1]($this);

				}elseif (\is_callable($failureFunction)) {
					$return .= $failureFunction($this);
				}
			}
		}

		return $return;
	}

	/**
	 * Set the render file
	 *
	 * @param $renderFile
	 */
	public function setCustomRenderer($renderFile)
	{
		$this->customRenderFile = $renderFile;
	}

	/**
	 * Create a new Form Element
	 *
	 * @param string $type   Type of element to create.
	 * @param array  $params Additional Params to pass to elements constructor
	 * @throws \Nomad\Exception\Form
	 */
	public function createElement($type, $params = array())
	{
		if (!isset($params['name'])) {
			throw new Exception\Form("'name' must be a parameter in createElement(\$name, \$params = array())");
		}

		$elementName = $params['name'];
		unset($params['name']);

		$params = array_merge(['elementWrapper' => $this->_elementWrapperTag, 'elementType' => $type], $params);

		if (!isset($params['attributes']['id'])) {
			$params['attributes']['id'] = $this->name . '-' . $elementName;
		}

		/**
		 * Check for a user created form element class first.
		 * This can (and is allowed) to overwrite nomad classes.
		 * If not found, then try a for a nomad element class, throw exception if no Nomad class exists either
		 */
		$nomadType = "Nomad\\Form\\Element\\" . ucfirst($type);
		if (class_exists($type)) {

			$this->_elements[$elementName] = new $type ($elementName, $params);
		}
		elseif (class_exists($nomadType)) {
			$this->_elements[$elementName] = new $nomadType ($elementName, $params);
		}
		else {
			throw new Exception\Form('Unknown form element type: ' . $type);
		}
		if ($type == 'submit') {
			$this->_submitButtonName = $elementName;
		}
	}

	/**
	 * @param      $elementName
	 * @param null $default
	 * @return null|\Nomad\Form\Element\AbstractElement
	 */
	public function getElement($elementName, $default = null)
	{
		if (isset($this->_elements[$elementName])) {
			$default = $this->_elements[$elementName];
		}

		return $default;
	}

	/**
	 * Returns all elements
	 *
	 * @return null|\Nomad\Form\Element\AbstractElement
	 */
	public function getElements()
	{
		return $this->_elements;
	}

	/**
	 * Set forms action
	 *
	 * @param $action
	 */
	public function setAction($action)
	{
		$this->action = $action;
	}

	/**
	 * Get form's action
	 *
	 * @return string
	 */
	public function getAction()
	{
		return $this->action;
	}

	/**
	 * @param $viewClassActionPath string Sets a custom view file to use. Annotated style string: 'Package\Executor\Name::method'
	 */
	public function setView($viewClassActionPath)
	{
		$this->_view = new View(array('view' => $viewClassActionPath));
	}

	/**
	 * Re-initialize form
	 *
	 * @throws Exception\Form
	 */
	public function reInitialize()
	{
		$this->_elements = array();
		$this->initialize();
	}

	/**
	 * @return array
	 */
	public function getValues()
	{
		$values = array();
		foreach ($this->_elements as $element) {
			if (!$element->ignore) {
				$values[$element->getName()] = $element->getValue();
			}
		}

		return $values;
	}

	/**
	 * Helper function specifically for csrf and file uploads where the keys would be out of order and generate a different form identifier for session
	 *
	 * @return array
	 */
	protected function _getSortedKeys()
	{
		$temp = $this->_elements;
		foreach ($temp as $key => $element) {

			if ($this->_isIgnoredClass($element)) {
				unset($temp[$key]);
			}
		}

		$keys = array_keys($temp);

		asort($keys);

		return $keys;
	}

	/**
	 * @param $element
	 * @return bool
	 */
	protected function _isIgnoredClass($element)
	{
		return in_array(get_class($element), $this->_ignoredCsrfElements);
	}

	/**
	 * Helper function to create the attributes of a tag
	 *
	 * @param $attributes
	 * @return string
	 */
	protected function _createAttributeString($attributes)
	{
		$html = '';
		if (is_array($attributes)) {
			foreach ($attributes as $attrName => $attrValue) {
				$html .= " {$attrName}='{$attrValue}'";
			}

			return ltrim($html);
		}
		else {
			return $attributes;
		}
	}
}