<?php
/**
 * This file is part of the NomadPhp Framework.
 *
 * (c) Mark Hillebert <mhillebert@nomadphp.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Nomad\Form;

/**
 * Class AbstractHandler
 *
 * @package Nomad\Form
 * @author  Mark Hillebert
 */
abstract class AbstractHandler
{
	/**
	 * Method to execute when form passes all validations
	 *
	 * @param $form
	 */
	public function onSuccess($form)
	{
	}

	/**
	 * Method to execute when for fails a validation
	 *
	 * @param $form
	 * @return mixed
	 */
	public function onFailure($form)
	{
		return $form->render();
	}
}